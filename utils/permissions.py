from rest_framework import status
from rest_framework.exceptions import APIException
from rest_framework.permissions import BasePermission, IsAuthenticated


# class CustomForbidden(APIException):
#     status_code = status.HTTP_403_FORBIDDEN
#     default_detail = "Add your custom error message here"

class IsEmailVerify(BasePermission):
    message = 'Email is not verify'

    def has_permission(self, request, view):
        # if not request.user.is_email_verify:
        #     raise CustomForbidden
        return request.user and request.user.is_email_verify
