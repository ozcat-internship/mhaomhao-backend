import json


def find_province(province_id):
    with open('config/address/provinces.json', 'r') as f:
        provinces = json.load(f)
    filter_provinces = filter(lambda x: x['id'] == province_id, provinces)
    return list(filter_provinces)


def find_district(district_id):
    with open('config/address/districts.json', 'r') as f:
        districts = json.load(f)
    filter_districts = filter(lambda x: x['id'] == district_id, districts)
    return list(filter_districts)


def find_sub_district(sub_district_id):
    with open('config/address/subdistricts.json', 'r') as f:
        sub_districts = json.load(f)
    filter_sub_districts = filter(lambda x: x['id'] == sub_district_id, sub_districts)
    return list(filter_sub_districts)


def find_postal_code(postal_codes_id):
    with open('config/address/postal_codes.json', 'r') as f:
        postal_codes = json.load(f)
    filter_postal_codes = filter(lambda x: x['id'] == postal_codes_id, postal_codes)
    return list(filter_postal_codes)


def is_find_province(province_id):
    list = find_province(province_id)
    return len(list) > 0


def is_find_district(district_id):
    list = find_district(district_id)
    return len(list) > 0


def is_find_sub_district(sub_district_id):
    list = find_sub_district(sub_district_id)
    return len(list) > 0


def is_find_postal_code(postal_codes_id):
    list = find_postal_code(postal_codes_id)
    return len(list) > 0
