from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

ITEMS_PER_PAGE = settings.ITEMS_PER_PAGE

def get_pagination_list(request, data):
  try:
      current_page = int(request.GET.get('page', 1))
  except:
      print("Remark 1*")
      current_page = 1
      traceback.print_exc()

  paginator = Paginator(data, ITEMS_PER_PAGE)
  try:
      content = paginator.page(current_page).object_list
  except PageNotAnInteger:
      print("Remark 2*")
      current_page = 1
      content = paginator.page(1)
  except EmptyPage:
      print("Remark 3*")
      current_page = paginator.num_pages
      content = []
  except:
      print("Remark 4*")
      traceback.print_exc()

  page = {
      'number_items': data.count(1),
      'items_per_page': ITEMS_PER_PAGE,
      'current_page': current_page,
      'number_pages': paginator.num_pages
  }

  return content, page