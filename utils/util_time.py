import pytz
import hashlib
from dateutil.parser import parse
from datetime import datetime
from django.utils import timezone


utc = pytz.UTC


def convert_datetime_to_isoformat(dt):
    dt_isoformat = None
    try:
        dt_isoformat = dt.isoformat()
    except:
        pass
    
    return dt_isoformat


def convert_datetime_to_timestamp(dt):
    dt_ts = None
    try:
        dt_ts = dt.strftime('%s')
    except:
        pass
    
    return dt_ts


def convert_date_time_str_to_datetime(text):
    dt = None
    try:
        dt = parse(text)
    except:
        pass
    
    return dt


def is_date_time_valid(date_start, date_end):
    is_order_date_valid = (date_start < date_end) if (date_start and date_end) else False
    
    try:
        is_future_date_valid = timezone.now() < date_start
    except:
        try:
            is_future_date_valid = datetime.now() < date_start
        except:
            is_future_date_valid = False
    
    return is_order_date_valid and is_future_date_valid


def gen_ref():
    hash = hashlib.sha1()
    hash.update(datetime.now().strftime('%s').encode('utf-8'))
    ref = hash.hexdigest()[:6]

    return ref
