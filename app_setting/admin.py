from django.contrib import admin
from app_setting.models import AppSetting

# Register your models here.
admin.site.register(AppSetting)