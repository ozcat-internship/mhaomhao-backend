from django.db import models

class AppSetting(models.Model):
  name = models.CharField(max_length=150, blank=False)
  value = models.CharField(max_length=150, blank=False)
  setting_type = models.CharField(max_length=150, null=True, blank=True)