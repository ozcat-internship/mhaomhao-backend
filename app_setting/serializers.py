from rest_framework import serializers
from app_setting.models import AppSetting

class AppSettingSerializer(serializers.ModelSerializer):
  class Meta:
        fields = '__all__'
        model = AppSetting