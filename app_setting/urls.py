from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from app_setting.views import AppSettingViewSet, AppSettingMap

schema_view = get_swagger_view(title='Auction API')
router = DefaultRouter()

router.register(r'', AppSettingViewSet, base_name='app_setting')

urlpatterns = [
    url(r'^docs/', schema_view),
    url(r'dict/', AppSettingMap.as_view())
]

urlpatterns += router.urls