from django.shortcuts import render
from rest_framework import viewsets
from app_setting.models import AppSetting
from app_setting.serializers import AppSettingSerializer
from rest_framework.views import APIView
from rest_framework.response import Response

class AppSettingViewSet(viewsets.ModelViewSet):
  serializer_class = AppSettingSerializer
  queryset = AppSetting.objects.all()

class AppSettingMap(APIView):
  def get(self, request):
    settings = AppSetting.objects.all()
    return Response(self.get_setting_dict(settings))

  def get_setting_dict(self, settings):
    setting_dict = {}
    for item in settings.iterator():
      setting_dict[item.name] = item.value
    return setting_dict