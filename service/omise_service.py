import omise
from pprint import pprint

omise.api_secret = 'skey_test_5dob0hzbzskw4t1ir3d'
omise.api_public = 'pkey_test_5dob0hzbo0yy06vgzv6'


def create_customer(email, card_id):
    print("create_customer", email, card_id)
    omise_customer = omise.Customer.create(
        description=email,
        email=email,
        card=card_id
    )
    print("success")
    return omise_customer


def add_card_to_customer(customer_id, card_id):
    omise_customer = omise.Customer.retrieve(customer_id)
    omise_customer.card = card_id
    omise_customer.update()
    return omise_customer


def remove_card_to_customer(customer_id, card_id):
    omise_customer = omise.Customer.retrieve(customer_id)
    card = omise_customer.cards.retrieve(card_id)
    card.destroy()
    return omise_customer


def retrieve_card_list(customer_id):
    omise_customer = omise.Customer.retrieve(customer_id)
    cards = omise_customer.cards
    return cards.data


def charge_card(customer_id, card, amount):
    charge = omise.Charge.create(
        amount=amount,
        currency="thb",
        customer=customer_id,
        card=card
    )
    return charge

def edit_card(customer_id, card, exp_month, exp_year, name):
    customer = omise.Customer.retrieve(customer_id)
    card = customer.cards.retrieve(card)
    card.update(
        expiration_month=exp_month,
        expiration_year=exp_year,
        name=name,
    )

# charge = charge_card("cust_test_5dxmpngedbwpuafhphc", "card_test_5dxmpjg866ghn1dy816", 2000)
# pprint(vars(charge))

# customer = create_customer("wuttipong.kpb@gmail.com", )
# pprint(vars(customer))
# pprint(customer.cards.data)

# customer = remove_card_to_customer("cust_test_5dxmpngedbwpuafhphc", "card_test_5dxmpjg866ghn1dy816")
# pprint(vars(customer))
# .\curl.exe https://vault.omise.co/tokens \
#   -X POST \
#   -u pkey_test_5dob0hzbo0yy06vgzv6 \
#   -d "card[name]=Somchai Prasert" \
#   -d "card[number]=4242424242424242" \
#   -d "card[expiration_month]=12" \
#   -d "card[expiration_year]=2021" \
#   -d "card[city]=Bangkok" \
#   -d "card[postal_code]=10320" \
#   -d "card[security_code]=123"



# .\curl.exe https://api.omise.co/customers/cust_test_5dxmpngedbwpuafhphc \
#   -X PATCH \
#   -u skey_test_5dob0hzbzskw4t1ir3d \
#   -d "card=tokn_test_5e0sihdmgsvjmg3ukd2"