def get_username(backend, user, response, *args, **kwargs):
    if backend.name == 'facebook':
        if user:
            username = user.username
        else:
            username = response.get('email')
        return {'username': username}