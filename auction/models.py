import os
from django.db import models
from datetime import datetime
# from django.contrib.auth.models import AbstractUser

from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


class FarmStandard(models.Model):
    name = models.TextField(unique=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "FarmStandard"
        verbose_name_plural = "FarmStandards"
        db_table = "farm_standards"
        ordering = ['id']

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class FruitType(models.Model):
    name = models.TextField(unique=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "FruitType"
        verbose_name_plural = "FruitTypes"
        db_table = "auction_fruit_types"
        ordering = ['id']

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class FruitBreed(models.Model):
    fruit_type = models.ForeignKey(FruitType, null=True, blank=True, on_delete=models.SET_NULL, related_name='fruit_breeds')

    name = models.TextField(unique=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "FruitBreed"
        verbose_name_plural = "FruitBreeds"
        db_table = "auction_fruit_breeds"
        ordering = ['id']

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class Farm(models.Model):
    indexes = [
        models.Index(fields=['updated_at'])
    ]
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)
    fruit_breed = models.ForeignKey(FruitBreed, null=True, blank=True, on_delete=models.SET_NULL)
    farm_standard = models.ForeignKey(FarmStandard, null=True, blank=True, on_delete=models.SET_NULL)

    name = models.TextField(unique=True)

    # location
    address = models.TextField(null=True, blank=True)
    # district = models.TextField(null=True, blank=True)
    # province = models.TextField(null=True, blank=True)
    road = models.TextField(null=True, blank=False)
    country = models.TextField(null=True, blank=False)
    province_id = models.PositiveIntegerField(null=True, blank=True)
    district_id = models.PositiveIntegerField(null=True, blank=True)
    sub_district_id = models.PositiveIntegerField(null=True, blank=True)
    postal_code_id = models.PositiveIntegerField(null=True, blank=True)
    phone_number = models.TextField(null=True, blank=False)

    # size
    num_area = models.FloatField(null=True, blank=True)
    num_tree = models.IntegerField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "Farm"
        verbose_name_plural = "Farms"
        db_table = "farms"
        ordering = ['-updated_at']

    def __unicode__(self):
        return '{} - {}'.format(self.name, self.user.username)

    def __str__(self):
        return '{} - {}'.format(self.name, self.user.username)


class AuctionBase(models.Model):
    farm = models.ForeignKey(Farm, null=True, blank=True, on_delete=models.SET_NULL)

    code = models.TextField(unique=True)
    
    is_collect_service = models.BooleanField(default=False)
    expected_harvest_output = models.FloatField(null=True, blank=True, default=0.0)
    expected_harvest_date_time_start = models.DateTimeField(null=True, blank=True)
    expected_harvest_date_time_end = models.DateTimeField(null=True, blank=True)
    
    date_time_start = models.DateTimeField(null=True, blank=True)
    date_time_end = models.DateTimeField(null=True, blank=True)
    
    price_start = models.FloatField(null=True, blank=True, default=0.0)
    price_current = models.FloatField(null=True, blank=True, default=0.0)
    price_end = models.FloatField(null=True, blank=True, default=0.0)
    
    guarantee_used = models.BooleanField(default=False)
    guarantee_price = models.FloatField(null=True, blank=True, default=0.0)
    extra_condition = models.TextField(null=True, blank=True)
    
    # Draft OTP

    draft_farm = models.ForeignKey(Farm, null=True, blank=True, on_delete=models.SET_NULL, related_name='%(class)s_draft_farm')

    draft_is_collect_service = models.BooleanField(default=False)
    draft_expected_harvest_output = models.FloatField(null=True, blank=True, default=0.0)
    draft_expected_harvest_date_time_start = models.DateTimeField(null=True, blank=True)
    draft_expected_harvest_date_time_end = models.DateTimeField(null=True, blank=True)

    draft_date_time_start = models.DateTimeField(null=True, blank=True)
    draft_date_time_end = models.DateTimeField(null=True, blank=True)

    draft_price_start = models.FloatField(null=True, blank=True, default=0.0)

    draft_guarantee_used = models.BooleanField(default=False)
    draft_guarantee_price = models.FloatField(null=True, blank=True, default=0.0)
    draft_extra_condition = models.TextField(null=True, blank=True)

    # TODO: add more status here following base code
    DRAFT = -1
    PRE = 0
    OPEN = 1
    CLOSE = 10
    DELIVERY = 50
    ISSUE = 60
    COMPLETE = 80
    DELETE = 999
    OPEN_STATUS_CHOICES = (
        (DRAFT, 'draft'),
        (PRE, 'pre'),
        (OPEN, 'open'),
        (CLOSE, 'close'),
        (DELIVERY, 'delivery'),
        (ISSUE, 'issue'),
        (COMPLETE, 'delivery'),
        (DELETE, 'delete'),
    )
    status = models.IntegerField(choices=OPEN_STATUS_CHOICES, default=DRAFT)

    # TODO: stamp raw value log -prefix as raw_
    # auction owner user
    # farm

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "AuctionBase"
        verbose_name_plural = "AuctionBases"
        db_table = "auction_bases"
        ordering = ['id']

    def __unicode__(self):
        return "{} - {}".format(self.draft_farm, self.draft_date_time_start, self.draft_price_start)

    def __str__(self):
        return "{} - {}".format(self.draft_farm, self.draft_date_time_start, self.draft_price_start)
    
    @staticmethod
    def generate_code():
        return "temp_code_" + datetime.now().strftime('%s')


class AuctionTransaction(models.Model):
    auction_base = models.ForeignKey(AuctionBase, null=True, blank=True, on_delete=models.SET_NULL)
    # bid user (user who make transaction as sell/buy)
    bid_user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)

    bid_price = models.FloatField(null=True, blank=True, default=0.0)
    bid_date_time = models.DateTimeField(default=None)

    # TODO: add more status here following base code
    DRAFT_BUY = 10
    BUY = 20
    DRAFT_SELL = 50
    SELL = 60
    DELETE = 999
    TRANSACTION_CHOICES = (
        (DRAFT_BUY, 'draft_buy'),
        (BUY, 'buy'),
        (DRAFT_SELL, 'draft_sell'),
        (SELL, 'sell'),
        (DELETE, 'delete'),
    )
    transaction_status = models.IntegerField(choices=TRANSACTION_CHOICES, default=DRAFT_BUY)

    # TODO: stamp raw value log -prefix as raw_
    # auction owner user
    # farm
    # auction base
    # auction bid user

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "AuctionTransaction"
        verbose_name_plural = "AuctionTransactions"
        db_table = "auction_transactions"
        ordering = ['id']
        
    def __unicode__(self):
        return "{} - {} - {}".format(self.bid_price, self.bid_user, self.auction_base)

    def __str__(self):
        return "{} - {} - {}".format(self.bid_price, self.bid_user, self.auction_base)


class AuctionDelivery(models.Model):
    # TODO: redundant AuctionTransaction as top 5 for serve AuctionDelivery progress
    auction_transaction = models.ForeignKey(AuctionTransaction, null=True, blank=True, on_delete=models.SET_NULL)

    num_queue = models.IntegerField(null=True, blank=True, default=1)

    # TODO: add more status here following base code
    WAIT = 0
    PAYMENT = 10
    PARTIAL_PAYMENT = 11
    FULL_PAYMENT = 12
    APPOINTMENT = 40
    ISSUE = 50
    ISSUE_SELLER = 51
    ISSUE_BUYER = 52
    COMPLETE = 70
    COMPLETE_ISSUE_SELLER = 71
    COMPLETE_ISSUE_BUYER = 72
    COMPLETE_SUCCESS = 80
    DELETE = 99
    DELIVERY_STATUS_CHOICES = (
        (WAIT, 'WAIT'),
        (PAYMENT, 'PAYMENT'),
        (PARTIAL_PAYMENT, 'PARTIAL_PAYMENT'),
        (FULL_PAYMENT, 'FULL_PAYMENT'),
        (APPOINTMENT, 'APPOINTMENT'),
        (ISSUE, 'ISSUE'),
        (ISSUE_SELLER, 'ISSUE_SELLER'),
        (ISSUE_BUYER, 'ISSUE_BUYER'),
        (COMPLETE, 'COMPLETE'),
        (COMPLETE_ISSUE_SELLER, 'COMPLETE_ISSUE_SELLER'),
        (COMPLETE_ISSUE_BUYER, 'COMPLETE_ISSUE_BUYER'),
        (COMPLETE_SUCCESS, 'COMPLETE_SUCCESS'),
        (DELETE, 'DELETE'),
    )
    delivery_status = models.IntegerField(choices=DELIVERY_STATUS_CHOICES, default=WAIT)

    # TODO: stamp raw value log -prefix as raw_
    # auction owner user
    # farm
    # auction base
    # auction bid user
    # auction transaction

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "AuctionDelivery"
        verbose_name_plural = "AuctionDeliveries"
        db_table = "auction_deliveries"
        ordering = ['id']

    def __unicode__(self):
        return "{}".format(self.id)

    def __str__(self):
        return "{}".format(self.id)


class Favorite(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)
    auction_base = models.ForeignKey(AuctionBase, null=True, blank=True, on_delete=models.SET_NULL)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "Favorite"
        verbose_name_plural = "Favorites"
        db_table = "favorites"
        ordering = ['id']

    def __unicode__(self):
        return '{} - {}'.format(self.user, self.auction_base)

    def __str__(self):
        return '{} - {}'.format(self.user, self.auction_base)


class Job(models.Model):
    # limit only 1 job / user
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)

    # TODO: add more job field here
    name = models.TextField(unique=True)
    extra_condition = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "Job"
        verbose_name_plural = "Jobs"
        db_table = "jobs"
        ordering = ['id']

    def __unicode__(self):
        return '{} - {}'.format(self.user, self.name)

    def __str__(self):
        return '{} - {}'.format(self.user, self.name)


class PaymentRequest(models.Model):
    # แจ้งโอน, ไม่ใช่โอน หรือการจ่ายตัง
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)

    auction_base_code = models.TimeField(unique=True)
    auction_base = models.ForeignKey(AuctionBase, null=True, blank=True, on_delete=models.SET_NULL)

    payment_date = models.DateField(null=True, blank=True)
    payment_time = models.TimeField(null=True, blank=True)

    # TODO: PaymentLog from Admin operation
    USER = 1
    ADMIN = 2
    SOURCE_CHOICES = (
        (USER, 'USER'),
        (ADMIN, 'ADMIN'),
    )
    source = models.IntegerField(choices=SOURCE_CHOICES, default=USER)

    TRANSFER_GENERAL = 10
    TRANSFER_PARTIAL_PAYMENT = 11
    TRANSFER_FULL_PAYMENT = 12
    GUARANTEE_DEPOSIT = 21
    GUARANTEE_WITHDRAW = 22
    TYPE_CHOICES = (
        (TRANSFER_GENERAL, 'TRANSFER_GENERAL'),
        (TRANSFER_PARTIAL_PAYMENT, 'TRANSFER_PARTIAL_PAYMENT'),
        (TRANSFER_FULL_PAYMENT, 'TRANSFER_FULL_PAYMENT'),
        (GUARANTEE_DEPOSIT, 'GUARANTEE_DEPOSIT'),
        (GUARANTEE_WITHDRAW, 'GUARANTEE_WITHDRAW'),
    )
    type = models.IntegerField(choices=TYPE_CHOICES, default=TRANSFER_GENERAL)

    REQUEST = 10
    REVIEW = 60
    COMPLETE = 90
    CANCEL = 91
    DELETE = 999
    PAYMENT_REQUEST_STATUS_CHOICE = (
        (REQUEST, 'REQUEST'),
        (REVIEW, 'REVIEW'),
        (COMPLETE, 'COMPLETE'),
        (CANCEL, 'CANCEL'),
        (DELETE, 'DELETE'),
    )
    status = models.IntegerField(choices=PAYMENT_REQUEST_STATUS_CHOICE, default=REQUEST)

    # TODO: add more status here following bank code
    payment_bank = models.IntegerField(null=True, blank=True, default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "PaymentRequest"
        verbose_name_plural = "PaymentRequests"
        db_table = "payment_requests"
        ordering = ['id']

    def __unicode__(self):
        return '{}'.format(self.id)

    def __str__(self):
        return '{}'.format(self.id)


class OTP(models.Model):
    ref = models.TextField()
    otp = models.TextField()
    
    NONE = 0
    CREATE = 10
    UPDATE = 20
    TRANSACTION_TYPE = (
        (NONE, 'NONE'),
        (CREATE, 'CREATE'),
        (UPDATE, 'UPDATE'),
    )
    transaction_type = models.IntegerField(choices=TRANSACTION_TYPE, default=NONE)

    AUCTION_BASE = 10
    AUCTION_TRANSACTION = 20
    TRANSACTION_MODEL = (
        (AUCTION_BASE, 'AUCTION_BASE'),
        (AUCTION_TRANSACTION, 'AUCTION_TRANSACTION'),
    )
    transaction_model = models.IntegerField(choices=TRANSACTION_MODEL, default=NONE)

    auction_base = models.ForeignKey(AuctionBase, null=True, blank=True, on_delete=models.SET_NULL)
    auction_transaction = models.ForeignKey(AuctionTransaction, null=True, blank=True, on_delete=models.SET_NULL)

    STATUS_INACTIVE = 0
    STATUS_ACTIVE = 1
    STATUS_VERIFY = 10
    STATUS_EXPIRED = 99
    STATUS_CHOICE = (
        (STATUS_INACTIVE, 'STATUS_INACTIVE'),
        (STATUS_ACTIVE, 'STATUS_ACTIVE'),
        (STATUS_VERIFY, 'STATUS_VERIFY'),
        (STATUS_EXPIRED, 'STATUS_EXPIRED'),
    )
    status = models.IntegerField(choices=STATUS_CHOICE, default=NONE)
    
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)

    expired_at = models.DateTimeField()
    
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "otp"
        verbose_name_plural = "otps"
        db_table = "otps"
        ordering = ['id']

    def __unicode__(self):
        return self.ref

    def __str__(self):
        return self.ref
