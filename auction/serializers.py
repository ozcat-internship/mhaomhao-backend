from rest_framework import serializers

# from django.contrib.auth.models import User
from django.conf import settings
from auction.models import FruitType, FruitBreed, Farm, AuctionBase, AuctionTransaction, Favorite, FarmStandard
from auction.models_media_helper import MediaFarm, MediaAuctionBase, MediaPaymentRequest, MediaJob
from account.models import User
from utils import base36, address as address_util

# class PostSerializer(serializers.ModelSerializer):
#     class Meta:
#         fields = ('id', 'title', 'content', 'created_at', 'updated_at',)
#         model = Post

# class UniversitySerializer(serializers.ModelSerializer):
#     # students = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
#     # students = serializers.RelatedField(many=True)
#     # students = serializers.SlugRelatedField(many=True, read_only=True, slug_field='first_name')

#     class Meta:
#         fields = '__all__'
#         # fields = ('id', 'name', 'students', 'created_at', 'updated_at',)
#         model = University

# class StudentSerializer(serializers.ModelSerializer):
#     university = UniversitySerializer()

#     class Meta:
#         # fields = '__all__'
#         fields = ('id', 'first_name', 'last_name', 'university', 'created_at', 'updated_at',)
#         model = Student


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = User


class FruitBreedSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = FruitBreed


class FruitTypeSerializer(serializers.ModelSerializer):
    fruit_breeds = FruitBreedSerializer(many=True, read_only=True)
    class Meta:
        fields = '__all__'
        model = FruitType


class FarmStandardSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = FarmStandard


class FarmSerializer(serializers.ModelSerializer):
    class Meta:
        model = Farm
        fields = ('id', 'user', 'fruit_breed', 'farm_standard',
                  'address', 'road', 'country',
                  'province_id', 'district_id',
                  'sub_district_id', 'postal_code_id',
                  'name', 'num_area', 'num_tree', 'phone_number')

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        provinces = dict(enumerate(address_util.find_province(instance.province_id)))
        districts = dict(enumerate(address_util.find_district(instance.district_id)))
        sub_districts = dict(enumerate(address_util.find_sub_district(instance.sub_district_id)))
        postal_codes = dict(enumerate(address_util.find_postal_code(instance.postal_code_id)))
        print("----address to_representration-----")
        ret['province'] = provinces.get(0, {}).get('name', {}) or None
        ret['districts'] = districts.get(0, {}).get('name', {}) or None
        ret['sub_districts'] = sub_districts.get(0, {}).get('name', {}) or None
        ret['postal_code'] = postal_codes.get(0, {}).get('code') or None
        return ret

    def create(self, validated_data):
        farm = Farm.objects.create(**validated_data)

        user = self.context.get('user', None)
        farm.user = user
        farm.save()

        print('POST: farm: {} was created'.format(farm))

        return farm

    def update(self, instance, validated_data):
        farm, created = Farm.objects.update_or_create(
            id=instance.id,
            defaults=validated_data)

        user = self.context.get('user', None)
        farm.user = user
        farm.save()

        print('POST: farm: {} was created: {}'.format(farm, created))

        return farm

    def validate(self, attrs):
        if not self.context.get('user'):
            raise serializers.ValidationError({'detail': 'user not provide'})

        for key, value in attrs.items():
            if key in ['user']:
                continue

            if (value is None) or (value == ''):
                raise serializers.ValidationError({'detail': '{} can not empty'.format(key)})

        return attrs


class AuctionBaseSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = AuctionBase


class AuctionTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = AuctionTransaction


class MediaFarmSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = MediaFarm


class MediaAuctionBaseSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = MediaAuctionBase


class FavoriteSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Favorite
