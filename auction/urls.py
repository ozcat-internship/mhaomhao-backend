from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter
from rest_framework_swagger.views import get_swagger_view

from auction.views import system_farm, user_auction_otp, user_farm, user_farm_media, user_auction, user_auction_media, \
    system_auctions, user_bid, user_bid_otp
from auction.views.custom_auth_token import CustomAuthToken
from auction.views.view_sets import FruitTypeViewSet, FruitBreedViewSet, FarmStandardViewSet, MediaFarmViewSet, \
    AdminFruitTypeViewSet, AdminFruitBreedViewSet, AdminFarmStandardViewSet

# from account.views import UserProfileAPIView
schema_view = get_swagger_view(title='Auction API')


router = DefaultRouter()
# router.register(r'posts', PostViewSet, base_name='posts')
# router.register(r'students', StudentViewSet, base_name='students')
# router.register(r'universities', UniversityViewSet, base_name='universities')

# router.register(r'users', UserProfileAPIView, base_name='users')
router.register(r'admin/fruit_types', AdminFruitTypeViewSet, base_name='admin_fruit_types')
router.register(r'admin/fruit_breeds', AdminFruitBreedViewSet, base_name='admin_fruit_breeds')
router.register(r'admin/farm_standard', AdminFarmStandardViewSet, base_name='admin_farm_standards')

router.register(r'fruit_types', FruitTypeViewSet, base_name='fruit_types')
router.register(r'fruit_breeds', FruitBreedViewSet, base_name='fruit_breeds')
router.register(r'farm_standard', FarmStandardViewSet, base_name='farm_standards')

# router.register(r'auctions/base', AuctionBaseViewSet, base_name='auction_bases')
# router.register(r'auctions/bid', AuctionTransactionViewSet, base_name='auctions')

router.register(r'media/farms', MediaFarmViewSet, base_name='media_farms')
# router.register(r'media/auctions/base', MediaAuctionBaseViewSet, base_name='media_auctions_base')

urlpatterns = [
    url(r'user/farms/$', user_farm.UserFarmAPIView.as_view(), name='user_farm_list'),
    url(r'user/farms/(?P<farm_id>[0-9]+)/$', user_farm.UserFarmAPIView.as_view(), name='user_farm_item'),
    url(r'user/farms/(?P<farm_id>[0-9]+)/medias/$', user_farm_media.UserFarmMediaAPIView.as_view(), name='user_farm_media_list'),
    url(r'user/farms/(?P<farm_id>[0-9]+)/medias/(?P<media_id>[0-9]+)/$', user_farm_media.UserFarmMediaAPIView.as_view(), name='user_farm_media_item'),
    
    url(r'user/auctions/$', user_auction.UserAuctionAPIView.as_view(), name='user_auction_list'),
    url(r'user/auctions/(?P<auction_id>[0-9]+)/$', user_auction.UserAuctionAPIView.as_view(), name='user_auction_item'),
    url(r'user/auctions/(?P<auction_id>[0-9]+)/medias/$', user_auction_media.UserAuctionMediaAPIView.as_view(), name='user_auction_media_list'),
    url(r'user/auctions/(?P<auction_id>[0-9]+)/medias/(?P<media_id>[0-9]+)/$', user_auction_media.UserAuctionMediaAPIView.as_view(), name='user_auction_media_item'),
    url(r'user/auctions/(?P<auction_id>[0-9]+)/otp/$', user_auction_otp.UserAuctionOTPAPIView.as_view(), name='user_auction_otp_item'),
    
    url(r'user/bid/(?P<auction_id>[0-9]+)/$', user_bid.UserBidAPIView.as_view(), name='user_bid_item'),
    url(r'user/bid/(?P<auction_transaction_id>[0-9]+)/otp/$', user_bid_otp.UserBidOTPAPIView.as_view(), name='user_bid_otp_item'),

    url(r'farms/$', system_farm.FarmAPIView.as_view(), name='system_farm_list'),
    url(r'farms/(?P<farm_id>[0-9]+)/$', system_farm.FarmAPIView.as_view(), name='system_farm_item'),
    
    url(r'auctions/$', system_auctions.AuctionListView.as_view(), name='system_auction_list'),
    url(r'auctions/(?P<auction_id>[0-9]+)/$', system_auctions.AuctionListView.as_view(), name='system_auction_item'),
    # url(r'^auctions/current', AuctionListView.as_view(), name='auctions'),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    # url(r'^api-token-auth/', views.obtain_auth_token),
    url(r'^api-token-auth/', CustomAuthToken.as_view()),
    url(r'^docs/', schema_view),
]

urlpatterns += router.urls
