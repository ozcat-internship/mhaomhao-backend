import os

from django.conf import settings
from django.db import models
from auction.models import Farm, AuctionBase, PaymentRequest, Job
from utils.util_time import convert_datetime_to_isoformat


def get_farm_image_path(instance, filename):
    return os.path.join('public/farm', filename)


class MediaFarm(models.Model):
    farm = models.ForeignKey(Farm, null=True, blank=True, on_delete=models.CASCADE)

    path = models.ImageField(upload_to=get_farm_image_path, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "MediaFarm"
        verbose_name_plural = "MediaFarms"
        db_table = "media_farms"
        ordering = ['id']

    def __unicode__(self):
        return "{} - {}".format(self.farm, self.path)

    def __str__(self):
        return "{} - {}".format(self.farm, self.path)
    
    @classmethod
    def get_media_farm_response_by_farm_id(cls, farm_id):
        filter_dict = {
            'deleted_at': None,
            'farm_id': farm_id,
        }
        media_farm_values = MediaFarm.objects.filter(**filter_dict)
        
        media_farm_list = []
        for media_farm_dict in media_farm_values:
            media_farm_list.append({
                'id': media_farm_dict.id,
                'farm_id': media_farm_dict.farm.id,
                'path': media_farm_dict.path.url,
                
                'created_at': convert_datetime_to_isoformat(media_farm_dict.created_at),
                'updated_at': convert_datetime_to_isoformat(media_farm_dict.updated_at),
                'deleted_at': convert_datetime_to_isoformat(media_farm_dict.deleted_at),
            })
        
        return media_farm_list


def get_auction_base_image_path(instance, filename):
    return os.path.join('public/auction_base', filename)


class MediaAuctionBase(models.Model):
    auction_base = models.ForeignKey(AuctionBase, null=True, blank=True, on_delete=models.CASCADE)

    path = models.ImageField(upload_to=get_auction_base_image_path, null=True, blank=True, default=None)
    draft_path = models.ImageField(upload_to=get_auction_base_image_path, null=True, blank=True, default=None)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "MediaAuctionBase"
        verbose_name_plural = "MediaAuctionBases"
        db_table = "media_auction_bases"
        ordering = ['id']

    def __unicode__(self):
        return "{} - {}".format(self.auction_base, self.draft_path)

    def __str__(self):
        return "{} - {}".format(self.auction_base, self.draft_path)
    
    @classmethod
    def get_media_auction_base_response_by_auction_id(cls, auction_base_id):
        filter_dict = {
            'deleted_at': None,
            'auction_base_id': auction_base_id
        }
        exclude_filter_dict = {
            'path__isnull': True,
            'path': '',
        }
        media_auction_base_values = MediaAuctionBase.objects.filter(**filter_dict).exclude(**exclude_filter_dict)
        
        media_auction_base_list = []
        for media_auction_base_dict in media_auction_base_values:
            if media_auction_base_dict.path:
                media_auction_base_list.append({
                    'id': media_auction_base_dict.id,
                    'auction_base_id': media_auction_base_dict.auction_base.id,
                    'path': media_auction_base_dict.path.url,
                    
                    'created_at': convert_datetime_to_isoformat(media_auction_base_dict.created_at),
                    'updated_at': convert_datetime_to_isoformat(media_auction_base_dict.updated_at),
                    'deleted_at': convert_datetime_to_isoformat(media_auction_base_dict.deleted_at),
                })
        
        return media_auction_base_list


def get_payment_request_image_path(instance, filename):
    return os.path.join('public/payment_request', filename)


class MediaPaymentRequest(models.Model):
    payment_request = models.ForeignKey(PaymentRequest, null=True, blank=True, on_delete=models.CASCADE)

    path = models.ImageField(upload_to=get_payment_request_image_path, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "MediaPaymentRequest"
        verbose_name_plural = "MediaPaymentRequests"
        db_table = "media_payment_requests"
        ordering = ['id']

    def __unicode__(self):
        return "{} - {}".format(self.payment_request, self.path)

    def __str__(self):
        return "{} - {}".format(self.payment_request, self.path)


def get_job_image_path(instance, filename):
    return os.path.join('public/job', filename)


class MediaJob(models.Model):
    job = models.ForeignKey(Job, null=True, blank=True, on_delete=models.CASCADE)

    path = models.ImageField(upload_to=get_job_image_path, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted_at = models.DateTimeField(null=True, blank=True, default=None)

    class Meta:
        verbose_name = "MediaJob"
        verbose_name_plural = "MediaJobs"
        db_table = "media_jobs"

    def __unicode__(self):
        return "{} - {}".format(self.job, self.path)

    def __str__(self):
        return "{} - {}".format(self.job, self.path)

