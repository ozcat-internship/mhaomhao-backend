from django.contrib import admin
from auction.models import AuctionBase, AuctionTransaction, Farm, FruitType, FruitBreed, Favorite, Job, PaymentRequest, AuctionDelivery, FarmStandard, OTP
from auction.models_media_helper import MediaFarm, MediaAuctionBase, MediaPaymentRequest, MediaJob

admin.site.register(FruitType)
admin.site.register(FruitBreed)
admin.site.register(FarmStandard)
admin.site.register(Farm)
admin.site.register(MediaFarm)
admin.site.register(AuctionBase)
admin.site.register(MediaAuctionBase)
admin.site.register(AuctionTransaction)
admin.site.register(AuctionDelivery)
admin.site.register(Favorite)
admin.site.register(Job)
admin.site.register(MediaJob)
admin.site.register(PaymentRequest)
admin.site.register(MediaPaymentRequest)
admin.site.register(OTP)

# admin.site.register(User)
