# Generated by Django 2.1.5 on 2019-05-14 17:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auction', '0015_auto_20190512_0047'),
    ]

    operations = [
        migrations.AddField(
            model_name='otp',
            name='status',
            field=models.IntegerField(choices=[(1, 'STATUS_ACTIVE'), (10, 'STATUS_ENABLE'), (80, 'STATUS_DISABLE'), (99, 'STATUS_EXPIRED')], default=0),
        ),
        migrations.AlterField(
            model_name='otp',
            name='ref',
            field=models.TextField(),
        ),
    ]
