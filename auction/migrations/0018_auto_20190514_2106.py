# Generated by Django 2.1.5 on 2019-05-14 21:06

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auction', '0017_auto_20190514_1825'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='auctionbase',
            options={'ordering': ['id'], 'verbose_name': 'AuctionBase', 'verbose_name_plural': 'AuctionBases'},
        ),
        migrations.AlterModelOptions(
            name='auctiondelivery',
            options={'ordering': ['id'], 'verbose_name': 'AuctionDelivery', 'verbose_name_plural': 'AuctionDeliveries'},
        ),
        migrations.AlterModelOptions(
            name='auctiontransaction',
            options={'ordering': ['id'], 'verbose_name': 'AuctionTransaction', 'verbose_name_plural': 'AuctionTransactions'},
        ),
        migrations.AlterModelOptions(
            name='favorite',
            options={'ordering': ['id'], 'verbose_name': 'Favorite', 'verbose_name_plural': 'Favorites'},
        ),
        migrations.AlterModelOptions(
            name='job',
            options={'ordering': ['id'], 'verbose_name': 'Job', 'verbose_name_plural': 'Jobs'},
        ),
        migrations.AlterModelOptions(
            name='mediaauctionbase',
            options={'ordering': ['id'], 'verbose_name': 'MediaAuctionBase', 'verbose_name_plural': 'MediaAuctionBases'},
        ),
        migrations.AlterModelOptions(
            name='mediafarm',
            options={'ordering': ['id'], 'verbose_name': 'MediaFarm', 'verbose_name_plural': 'MediaFarms'},
        ),
        migrations.AlterModelOptions(
            name='mediapaymentrequest',
            options={'ordering': ['id'], 'verbose_name': 'MediaPaymentRequest', 'verbose_name_plural': 'MediaPaymentRequests'},
        ),
        migrations.AlterModelOptions(
            name='otp',
            options={'ordering': ['id'], 'verbose_name': 'otp', 'verbose_name_plural': 'otps'},
        ),
        migrations.AlterModelOptions(
            name='paymentrequest',
            options={'ordering': ['id'], 'verbose_name': 'PaymentRequest', 'verbose_name_plural': 'PaymentRequests'},
        ),
    ]
