from datetime import datetime
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db import IntegrityError

from django.http import JsonResponse
from auction.models import Farm
from auction.models_media_helper import MediaFarm
from utils.util_time import convert_datetime_to_isoformat
from utils import base36, address as address_util
from auction.serializers import FarmSerializer

def get_farm_response(farm_dict):
    provinces = dict(enumerate(address_util.find_province(farm_dict.get('province_id'))))
    districts = dict(enumerate(address_util.find_district(farm_dict.get('district_id'))))
    sub_districts = dict(enumerate(address_util.find_sub_district(farm_dict.get('sub_district_id'))))
    postal_codes = dict(enumerate(address_util.find_postal_code(farm_dict.get('postal_code_id'))))
    return {
        'id': farm_dict.get('id'),
        
        # TODO: convert address info
        'address': farm_dict.get('address'),
        'road': farm_dict.get('road'),
        'sub_district_id': farm_dict.get('sub_district_id'),
        'district_id': farm_dict.get('district_id'),
        'province_id': farm_dict.get('province_id'),
        'postal_code_id': farm_dict.get('postal_code_id'),
        'country': farm_dict.get('country'),
        
        'farm_standard': farm_dict.get('farm_standard__name'),
        'fruit_breed': farm_dict.get('fruit_breed__name'),
        'fruit_type': farm_dict.get('fruit_breed__fruit_type__name'),
        
        'name': farm_dict.get('name'),
        'num_area': farm_dict.get('num_area'),
        'num_tree': farm_dict.get('num_tree'),
        'phone_number': farm_dict.get('phone_number'),
        
        'created_at': convert_datetime_to_isoformat(farm_dict.get('created_at')),
        'updated_at': convert_datetime_to_isoformat(farm_dict.get('updated_at')),
        'deleted_at': convert_datetime_to_isoformat(farm_dict.get('deleted_at')),

        'province': provinces.get(0, {}).get('name', {}) or None,
        'districts': districts.get(0, {}).get('name', {}) or None,
        'sub_districts': sub_districts.get(0, {}).get('name', {}) or None,
        'postal_code': postal_codes.get(0, {}).get('code') or None,
    }


def get_farm_data_from_params(request):
    user_id = request.user.id
    params = request.data
    return {
        'user_id': user_id,
        
        'name': params.get('name'),
        'fruit_breed_id': params.get('fruit_breed_id'),
        'farm_standard_id': params.get('farm_standard_id'),
        
        'phone_number': str(params.get('phone_number', '')),
        
        # size
        'num_area': params.get('num_area', 0),
        'num_tree': params.get('num_tree', 0),
        
        # location
        'address': params.get('address'),
        'road': params.get('road'),
        'country': params.get('country'),
    
        'province_id': params.get('province_id'),
        'district_id': params.get('district_id'),
        'sub_district_id': params.get('sub_district_id'),
        'postal_code_id': params.get('postal_code_id'),
    }


class UserFarmAPIView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    
    def get(self, request, farm_id=None):
        filter_dict = {
            'user': request.user,
            'deleted_at': None
        }
        if farm_id:
            filter_dict['id'] = farm_id
        
        farm_values = Farm.objects.filter(**filter_dict).values(
            'address', 'country', 'created_at', 'deleted_at', 'district_id', 'farm_standard__name',
            'fruit_breed__name', 'fruit_breed__fruit_type__name', 'id', 'name', 'num_area', 'num_tree', 'phone_number',
            'postal_code_id', 'province_id', 'road', 'sub_district_id', 'updated_at')
        
        farm_list = []
        for farm_dict in farm_values:
            farm_data = get_farm_response(farm_dict)
            farm_data['medias'] = MediaFarm.get_media_farm_response_by_farm_id(farm_dict.get('id'))
            farm_list.append(farm_data)
        
        response = {
            'data': farm_list
        }
        return Response(response)

    def post(self, request):
        data_dict = get_farm_data_from_params(request)
        try:
            farm = Farm(**data_dict)
            farm.save()

            serializer = FarmSerializer(farm)
            return JsonResponse(serializer.data, safe=False, status=201)
        except:
            pass
        
        return Response('item already exist', status=400)

    def put(self, request, farm_id=None):
        data_dict = get_farm_data_from_params(request)
        try:
            filter_dict = {
                'id': farm_id,
                'user': request.user,
                'deleted_at': None,
            }
            farm = Farm.objects.filter(**filter_dict)
            farm.update(**data_dict)

            serializer = FarmSerializer(farm.first())
            return JsonResponse(serializer.data, safe=False, status=200)
        except:
            pass

        return Response('item not found', status=404)

    def delete(self, request, farm_id=None):
        try:
            filter_dict = {
                'user': request.user,
                'deleted_at': None,
                'id': farm_id,
            }
            farm = Farm.objects.filter(**filter_dict).first()
            farm.deleted_at = datetime.now()
            farm.save()
            
            return Response('success', status=200)
        
        except:
            pass

        return Response('item not found', status=404)
