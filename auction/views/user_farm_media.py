from datetime import datetime
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db import IntegrityError

from auction.models import Farm
from auction.models_media_helper import MediaFarm
from utils.util_time import convert_datetime_to_isoformat


class UserFarmMediaAPIView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    
    def post(self, request, farm_id=None):
        filter_dict = {
            'user': request.user,
            'deleted_at': None,
            'id': farm_id,
        }
        if Farm.objects.filter(**filter_dict).first() is None:
            return Response('farm not found', status=404)
        
        try:
            data_dict = {
                'farm_id': farm_id,
                'path': request.data.get('image'),
            }
            MediaFarm(**data_dict).save()
            
            return Response('success', status=201)
        
        except:
            pass
        
        return Response('something went wrong', status=400)

    def delete(self, request, farm_id=None, media_id=None):
        try:
            filter_dict = {
                'farm__user': request.user,
                'farm_id': farm_id,
                'id': media_id,
                'deleted_at': None,
            }
            media_farm = MediaFarm.objects.filter(**filter_dict).first()
            media_farm.deleted_at = datetime.now()
            media_farm.save()
        
            return Response('success', status=200)
        
        except:
            pass
    
        return Response('item not found', status=404)