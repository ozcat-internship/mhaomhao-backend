from datetime import datetime

from django.db.models import Max
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from auction.models import AuctionBase, AuctionTransaction


class UserBidAPIView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    
    def post(self, request, auction_id):
        param = request.data
        user = request.user
        
        # validate bid price
        try:
            bid_price = float(param.get('bid_price'))
        except:
            bid_price = None
        if bid_price is None or bid_price <= 0:
            return Response('incorrect bid price', status=404)
        
        # validate auction_base
        filter_dict = {
            'id': auction_id,
            'status__in': [AuctionBase.OPEN],
            'deleted_at': None,
        }
        exclude_filter_dict = {
            'farm__user': user,
        }
        auction_base = AuctionBase.objects.filter(**filter_dict).exclude(**exclude_filter_dict).first()
        if auction_base is None:
            return Response('auction not found', status=404)
        
        # get current bid price
        filter_dict = {
            'auction_base': auction_base,
            'transaction_status__in': [AuctionTransaction.BUY],
            'deleted_at': None,
        }
        price = AuctionTransaction.objects.filter(**filter_dict).aggregate(bid_price=Max('bid_price'))
        current_transaction_price = price.get('bid_price')
        
        # validate current transaction price
        if current_transaction_price is not None:
            if bid_price <= current_transaction_price:
                return Response('bid price should more than current price', status=404)
        # validate minimum price
        if bid_price < auction_base.price_start:
            return Response('bid price should more than starting price', status=404)
        
        auction_transaction = AuctionTransaction(
            auction_base=auction_base,
            bid_user=user,
            bid_price=bid_price,
            bid_date_time=datetime.now(),
            transaction_status=AuctionTransaction.DRAFT_BUY
        )
        auction_transaction.save()
        
        response = {
            'data': {
                'id': auction_transaction.id,
                'auction_id': auction_transaction.auction_base.id,
                'bid_price': auction_transaction.bid_price,
            }
        }
        
        return Response(response, status=200)
