from django.contrib.auth.models import User
from rest_framework import viewsets, generics, permissions, status, views
from auction.models import (AuctionBase, AuctionTransaction, Farm, FruitType, FruitBreed, Favorite, FarmStandard)
from auction.models_media_helper import MediaFarm, MediaAuctionBase
from auction.serializers import (AuctionBaseSerializer,
                                 AuctionTransactionSerializer, FarmSerializer, FruitTypeSerializer,
                                 FruitBreedSerializer, MediaFarmSerializer,
                                 MediaAuctionBaseSerializer, FavoriteSerializer, FarmStandardSerializer)
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import Http404


# class PostList(generics.ListCreateAPIView):
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer


# class PostViewSet(viewsets.ModelViewSet):
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer


# class StudentViewSet(viewsets.ModelViewSet):
#     queryset = Student.objects.all()
#     serializer_class = StudentSerializer


# class UniversityViewSet(viewsets.ModelViewSet):
#     queryset = University.objects.all()
#     serializer_class = UniversitySerializer


# class UserViewSet(viewsets.ModelViewSet):
#     queryset = User.objects.all()
#     serializer_class = UserSerializer


class FruitTypeViewSet(viewsets.ModelViewSet):
    queryset = FruitType.objects.all()
    serializer_class = FruitTypeSerializer
    http_method_names = ['get']

class FruitBreedViewSet(viewsets.ModelViewSet):
    queryset = FruitBreed.objects.all()
    serializer_class = FruitBreedSerializer
    http_method_names = ['get']


class FarmStandardViewSet(viewsets.ModelViewSet):
    queryset = FarmStandard.objects.all()
    serializer_class = FarmStandardSerializer
    http_method_names = ['get']

class AdminFruitTypeViewSet(viewsets.ModelViewSet):
    queryset = FruitType.objects.all()
    serializer_class = FruitTypeSerializer


class AdminFruitBreedViewSet(viewsets.ModelViewSet):
    queryset = FruitBreed.objects.all()
    serializer_class = FruitBreedSerializer


class AdminFarmStandardViewSet(viewsets.ModelViewSet):
    queryset = FarmStandard.objects.all()
    serializer_class = FarmStandardSerializer

class FarmViewSet(viewsets.ModelViewSet):
    queryset = Farm.objects.all()
    serializer_class = FarmSerializer
"""
class FruitTypeViewSet(APIView):
    serializer_class = FruitTypeSerializer

    @classmethod
    def get(self, request, format=None):
        fruitType = FruitType.objects.all()
        serializer = self.serializer_class(fruitType, many=True)
        
        return Response(serializer.data)


class FruitBreedViewSet(APIView):
    serializer_class = FruitBreedSerializer

    @classmethod
    def get(self, request, format=None):
        fruitBreed = FruitBreed.objects.all()
        serializer = self.serializer_class(fruitBreed, many=True)
        
        return Response(serializer.data)

class FarmStandardViewSet(APIView):
    serializer_class = FarmStandardSerializer

    @classmethod
    def get(self, request, format=None):
        farmStandard = FarmStandard.objects.all()
        serializer = self.serializer_class(farmStandard, many=True)
        
        return Response(serializer.data)
"""
class FarmList(APIView):
    """
    List all snippets, or create a new snippet.
    """
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = FarmSerializer

    @classmethod
    def get_extra_actions(cls):
        return []

    def get(self, request, format=None):
        farm = Farm.objects.all()
        serializer = self.serializer_class(farm, many=True)
        
        return Response(serializer.data)

    def post(self, request, format=None):
        context = {
            'user': request.user,
            'request': request
        }
        serializer = self.serializer_class(
            data=request.data,
            context=context
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class FarmDetail(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = FarmSerializer

    @classmethod
    def get_extra_actions(cls):
        return []

    def get_object(self, pk):
        try:
            return Farm.objects.get(pk=pk)

        except Farm.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        farm = self.get_object(pk)
        serializer = self.serializer_class(farm)

        return Response(serializer.data)

    def put(self, request, pk, format=None):
        farm = self.get_object(pk)
        context = {
            'user': request.user,
            'request': request
        }
        serializer = self.serializer_class(
            farm,
            data=request.data,
            context=context)
        if serializer.is_valid():
            serializer.save()

            return Response(serializer.data)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        farm = self.get_object(pk)
        farm.delete()

        return Response(status=status.HTTP_204_NO_CONTENT)


class AuctionBaseViewSet(viewsets.ModelViewSet):
    queryset = AuctionBase.objects.all()
    serializer_class = AuctionBaseSerializer


class AuctionTransactionViewSet(viewsets.ModelViewSet):
    queryset = AuctionTransaction.objects.all()
    serializer_class = AuctionTransactionSerializer


class MediaFarmViewSet(viewsets.ModelViewSet):
    queryset = MediaFarm.objects.all()
    serializer_class = MediaFarmSerializer


class MediaAuctionBaseViewSet(viewsets.ModelViewSet):
    queryset = MediaAuctionBase.objects.all()
    serializer_class = MediaAuctionBaseSerializer


class FavoriteViewSet(viewsets.ModelViewSet):
    queryset = Favorite.objects.all()
    serializer_class = FavoriteSerializer
