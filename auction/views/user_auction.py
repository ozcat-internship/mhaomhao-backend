from datetime import datetime
from dateutil.parser import parse
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db import IntegrityError

from django.http import JsonResponse
from auction.models import Farm, AuctionBase
from auction.models_media_helper import MediaFarm, MediaAuctionBase
from utils.util_time import convert_datetime_to_isoformat, convert_date_time_str_to_datetime, is_date_time_valid
from utils import base36, address as address_util, pagination_util


def is_guarantee_require():
    # TODO: read from setting
    return False


def is_expected_harvest_date_time_valid(date_start, date_end):
    return is_date_time_valid(date_start, date_end)


def is_open_auction_date_time_valid(date_start, date_end):
    return is_date_time_valid(date_start, date_end)


def is_farm_id_valid(user_id, farm_id):
    filter_dict = {
        'id': farm_id,
        'user_id': user_id,
        'deleted_at': None
    }
    return Farm.objects.filter(**filter_dict).exists()


def is_guarantee_enough(user_id):
    # TODO: implement guarantee feature
    filter_dict = {
        'user_id': user_id,
        'deleted_at': None
    }
    
    return True

    # TODO: get_guarantee
    # TODO: make_guarantee


def get_auction_response(auction_dict):
    provinces = dict(enumerate(address_util.find_province(auction_dict.get('farm__province_id'))))
    districts = dict(enumerate(address_util.find_district(auction_dict.get('farm__district_id'))))
    sub_districts = dict(enumerate(address_util.find_sub_district(auction_dict.get('farm__sub_district_id'))))
    postal_codes = dict(enumerate(address_util.find_postal_code(auction_dict.get('farm__postal_code_id'))))
    
    return {
        "id": auction_dict.get("id"),
        "code": auction_dict.get("code"),
        
        "is_collect_service": auction_dict.get("is_collect_service", False),
        "expected_harvest_output": auction_dict.get("expected_harvest_output", 0),
        
        "expected_harvest_date_time_start": convert_datetime_to_isoformat(auction_dict.get("expected_harvest_date_time_start")),
        "expected_harvest_date_time_end": convert_datetime_to_isoformat(auction_dict.get("expected_harvest_date_time_end")),
        
        "date_time_start": convert_datetime_to_isoformat(auction_dict.get("date_time_start")),
        "date_time_end": convert_datetime_to_isoformat(auction_dict.get("date_time_end")),
        
        "price_start": auction_dict.get("price_start", 0),
        "price_current": auction_dict.get("price_current", 0),
        "guarantee_used": auction_dict.get("guarantee_used", 0),
        "guarantee_price": auction_dict.get("guarantee_price", 0),
        "extra_condition": auction_dict.get("extra_condition", ''),
        
        "status": auction_dict.get("status"),
        
        "farm": {
            "id": auction_dict.get("farm__id"),
        
            'farm_standard': auction_dict.get('farm__farm_standard__name'),
            'fruit_breed': auction_dict.get('farm__fruit_breed__name'),
            'fruit_type': auction_dict.get('farm__fruit_breed__fruit_type__name'),
            
            "name": auction_dict.get("farm__name"),
            "address": auction_dict.get("farm__address"),
            "country": auction_dict.get("farm__country"),
            "num_area": auction_dict.get("farm__num_area"),
            "num_tree": auction_dict.get("farm__num_tree"),
            "phone_number": auction_dict.get("farm__phone_number"),
            "road": auction_dict.get("farm__road"),
    
            'province': provinces.get(0, {}).get('name', {}) or None,
            'districts': districts.get(0, {}).get('name', {}) or None,
            'sub_districts': sub_districts.get(0, {}).get('name', {}) or None,
            'postal_code': postal_codes.get(0, {}).get('code') or None,
            
            "created_at": convert_datetime_to_isoformat(auction_dict.get("farm__created_at")),
            "updated_at": convert_datetime_to_isoformat(auction_dict.get("farm__updated_at")),
            "deleted_at": convert_datetime_to_isoformat(auction_dict.get("farm__deleted_at")),
        },
        
        'created_at': convert_datetime_to_isoformat(auction_dict.get('created_at')),
        'updated_at': convert_datetime_to_isoformat(auction_dict.get('updated_at')),
        'deleted_at': convert_datetime_to_isoformat(auction_dict.get('deleted_at')),
    }


def get_auction_data_from_params(request):
    params = request.data
    
    expected_harvest_date_time_start = convert_date_time_str_to_datetime(
        params.get('expected_harvest_date_time_start', None))
    expected_harvest_date_time_end = convert_date_time_str_to_datetime(
        params.get('expected_harvest_date_time_end', None))
    
    date_time_start = convert_date_time_str_to_datetime(params.get('date_time_start', None))
    date_time_end = convert_date_time_str_to_datetime(params.get('date_time_end', None))
    
    pre_data_dict = {
        'draft_farm_id': params.get('farm_id', None),
        
        'draft_is_collect_service': params.get('is_collect_service', False),
        'draft_expected_harvest_output': float(params.get('expected_harvest_output', 0)),
        'draft_expected_harvest_date_time_start': expected_harvest_date_time_start,
        'draft_expected_harvest_date_time_end': expected_harvest_date_time_end,
        
        'draft_date_time_start': date_time_start,
        'draft_date_time_end': date_time_end,
        
        'draft_price_start': float(params.get('price_start', 0)),
        'draft_price_current': None,
        'draft_price_end': None,
        
        'draft_guarantee_used': is_guarantee_require(),
        'draft_guarantee_price': None,
        
        'draft_extra_condition': params.get('extra_condition', None),
    }
    
    post_data_dict = {}
    for key in pre_data_dict.keys():
        if pre_data_dict[key] is not None:
            post_data_dict[key] = pre_data_dict[key]
    
    return post_data_dict
    

def get_auction(request, user, auction_id=None, is_post=False, is_update=False):
    if is_post:
        filter_dict = {'draft_farm__user': user, 'id': auction_id}
    elif is_update:
        filter_dict = {'draft_farm__user': user, 'id': auction_id}
    else:
        filter_dict = {
            'farm__user': user,
            'status__in': [AuctionBase.PRE, AuctionBase.OPEN],
            'deleted_at': None
        }

    if auction_id:
        filter_dict['id'] = auction_id

    order_by = '-created_at'
    if request.GET.get('order_by'):
        order_by = request.GET.get('order_by')

    if request.GET.get('status'):
        filter_dict['status'] = request.GET.get('status')

    if request.GET.get('fruit_type_id'):
        filter_dict['farm__fruit_breed__fruit_type_id'] = request.GET.get('fruit_type_id')

    if request.GET.get('fruit_breed_id'):
        filter_dict['farm__fruit_breed__id'] = request.GET.get('fruit_breed_id')

    if request.GET.get('farm_standard_id'):
        filter_dict['farm__farm_standard__id'] = request.GET.get('farm_standard_id')

    if request.GET.get('province_id'):
        filter_dict['farm__province_id'] = request.GET.get('province_id')

    if request.GET.get('district_id'):
        filter_dict['farm__district_id'] = request.GET.get('district_id')

    if request.GET.get('sub_district_id'):
        filter_dict['farm__sub_district_id'] = request.GET.get('sub_district_id')

    if request.GET.get('postal_code_id'):
        filter_dict['farm__postal_code_id'] = request.GET.get('postal_code_id')

    if request.GET.get('num_area'):
        filter_dict['farm__num_area'] = request.GET.get('num_area')

    if request.GET.get('num_tree'):
        filter_dict['farm__num_tree'] = request.GET.get('num_tree')

    auction_values = AuctionBase.objects.filter(**filter_dict).order_by(order_by).values(
        'id', 'code', 'is_collect_service',
        'expected_harvest_output',
        'expected_harvest_date_time_start', 'expected_harvest_date_time_end',
        'date_time_start', 'date_time_end',
        'price_start', 'price_current',
        'guarantee_used', 'guarantee_price', 'extra_condition',
        'status',
        'created_at', 'updated_at', 'deleted_at',

        'farm__address', 'farm__country', 'farm__district_id', 'farm__farm_standard__name',
        'farm__fruit_breed__name', 'farm__fruit_breed__fruit_type__name', 'farm__id', 'farm__name',
        'farm__num_area', 'farm__num_tree', 'farm__phone_number',
        'farm__postal_code_id', 'farm__province_id', 'farm__road', 'farm__sub_district_id',
        'farm__created_at', 'farm__updated_at', 'farm__deleted_at'
    )
    
    response = None

    auction_list = []
    for auction_dict in auction_values:
        auction_data = get_auction_response(auction_dict)
        auction_data['medias'] = MediaAuctionBase.get_media_auction_base_response_by_auction_id(auction_dict.get('id'))
        auction_list.append(auction_data)
        
    if auction_id:
        response = {}
        if len(auction_list) > 0:
            response = auction_list[0]
    else:
        response = auction_list
        
    return response


class UserAuctionAPIView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    
    def get(self, request, auction_id=None):
        auction_list = get_auction(request, request.user, auction_id)
        
        if auction_id:
            return Response(auction_list)
        
        auction_list, page = pagination_util.get_pagination_list(self.request, auction_list)

        print(auction_list)
        
        response = {
            'data': auction_list,
            'page': page
        }
        return Response(response)

    def post(self, request):
        data_dict = get_auction_data_from_params(request)
        user_id = request.user.id
        
        farm_id = data_dict.get('draft_farm_id')
        if is_farm_id_valid(user_id, farm_id) == False:
                return Response('invalid farm data', status=400)

        draft_date_time_start = data_dict.get('draft_date_time_start')
        draft_date_time_end = data_dict.get('draft_date_time_end')
        if is_open_auction_date_time_valid(draft_date_time_start, draft_date_time_end) == False:
            return Response('invalid auction date', status=400)

        draft_expected_harvest_date_time_start = data_dict.get('draft_expected_harvest_date_time_start')
        draft_expected_harvest_date_time_end = data_dict.get('draft_expected_harvest_date_time_end')
        if is_expected_harvest_date_time_valid(draft_expected_harvest_date_time_start, draft_expected_harvest_date_time_end) == False:
            return Response('invalid expected harvest date', status=400)

        data_dict['code'] = AuctionBase.generate_code()
        data_dict['status'] = AuctionBase.DRAFT
        
        try:
            auction_base = AuctionBase(**data_dict)
            auction_base.save()
            
            auction_list = get_auction(request, request.user, auction_base.id, is_post=True)
            
            return Response({'data': auction_list }, status=201)
        
        except:
            pass
    
        return Response('item already exist', status=400)

    def put(self, request, auction_id=None):
        data_dict = get_auction_data_from_params(request)
        user_id = request.user.id

        farm_id = data_dict.get('draft_farm_id')
        if farm_id:
            if is_farm_id_valid(user_id, farm_id) == False:
                return Response('invalid farm data', status=400)

        draft_date_time_start = data_dict.get('draft_date_time_start')
        draft_date_time_end = data_dict.get('draft_date_time_end')
        if draft_date_time_start and draft_date_time_end:
            if is_open_auction_date_time_valid(draft_date_time_start, draft_date_time_end) == False:
                return Response('invalid auction date', status=400)

        draft_expected_harvest_date_time_start = data_dict.get('draft_expected_harvest_date_time_start')
        draft_expected_harvest_date_time_end = data_dict.get('draft_expected_harvest_date_time_end')
        if draft_expected_harvest_date_time_start and draft_expected_harvest_date_time_end:
            if is_expected_harvest_date_time_valid(draft_expected_harvest_date_time_start, draft_expected_harvest_date_time_end) == False:
                return Response('invalid expected harvest date', status=400)
        
        filter_dict = {
            'id': auction_id,
            'draft_farm__user': request.user,
            'status__in': [AuctionBase.PRE, AuctionBase.DRAFT],
            'deleted_at': None,
        }
        auction_base = AuctionBase.objects.filter(**filter_dict)
        if auction_base.first() is None:
            return Response('item not found', status=404)
        
        if auction_base.first().status not in [AuctionBase.DRAFT, AuctionBase.PRE]:
            return Response('update not permit', status=401)

        try:
            auction_base.update(**data_dict)

            return Response({'data': get_auction(request, request.user, auction_id, is_update=True)}, status=200)

        except:
            pass

        return Response('item not found', status=404)

    def delete(self, request, auction_id=None):
        try:
            filter_dict = {
                'id': auction_id,
                'farm__user': request.user,
                'deleted_at': None,
            }
            auction_base = AuctionBase.objects.filter(**filter_dict).first()
            auction_base.deleted_at = datetime.now()
            auction_base.save()
            
            return Response('success', status=200)
        
        except:
            pass

        return Response('item not found', status=404)
