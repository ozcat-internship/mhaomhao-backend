from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from auction.models import AuctionBase
from auction.models_media_helper import MediaFarm, MediaAuctionBase

from utils.util_time import convert_datetime_to_isoformat, convert_date_time_str_to_datetime, is_date_time_valid
from utils import base36, address as address_util, pagination_util

def get_auction_response(auction_dict):
    provinces = dict(enumerate(address_util.find_province(auction_dict.get('farm__province_id'))))
    districts = dict(enumerate(address_util.find_district(auction_dict.get('farm__district_id'))))
    sub_districts = dict(enumerate(address_util.find_sub_district(auction_dict.get('farm__sub_district_id'))))
    postal_codes = dict(enumerate(address_util.find_postal_code(auction_dict.get('farm__postal_code_id'))))
    
    return {
        "id": auction_dict.get("id"),
        "code": auction_dict.get("code"),
        
        "is_collect_service": auction_dict.get("is_collect_service", False),
        "expected_harvest_output": auction_dict.get("expected_harvest_output", 0),
        
        "expected_harvest_date_time_start": convert_datetime_to_isoformat(auction_dict.get("expected_harvest_date_time_start")),
        "expected_harvest_date_time_end": convert_datetime_to_isoformat(auction_dict.get("expected_harvest_date_time_end")),
        
        "date_time_start": convert_datetime_to_isoformat(auction_dict.get("date_time_start")),
        "date_time_end": convert_datetime_to_isoformat(auction_dict.get("date_time_end")),
        
        "price_start": auction_dict.get("price_start", 0),
        "price_current": auction_dict.get("price_current", 0),
        "guarantee_used": auction_dict.get("guarantee_used", 0),
        "guarantee_price": auction_dict.get("guarantee_price", 0),
        "extra_condition": auction_dict.get("extra_condition", ''),
        
        "status": auction_dict.get("status"),
        
        "farm": {
            "id": auction_dict.get("farm__id"),
        
            'farm_standard': auction_dict.get('farm__farm_standard__name'),
            'fruit_breed': auction_dict.get('farm__fruit_breed__name'),
            'fruit_type': auction_dict.get('farm__fruit_breed__fruit_type__name'),
            
            "name": auction_dict.get("farm__name"),
            "address": auction_dict.get("farm__address"),
            "country": auction_dict.get("farm__country"),
            "num_area": auction_dict.get("farm__num_area"),
            "num_tree": auction_dict.get("farm__num_tree"),
            "phone_number": auction_dict.get("farm__phone_number"),
            "road": auction_dict.get("farm__road"),
    
            'province': provinces.get(0, {}).get('name', {}) or None,
            'districts': districts.get(0, {}).get('name', {}) or None,
            'sub_districts': sub_districts.get(0, {}).get('name', {}) or None,
            'postal_code': postal_codes.get(0, {}).get('code') or None,
            
            "created_at": convert_datetime_to_isoformat(auction_dict.get("farm__created_at")),
            "updated_at": convert_datetime_to_isoformat(auction_dict.get("farm__updated_at")),
            "deleted_at": convert_datetime_to_isoformat(auction_dict.get("farm__deleted_at")),
        },
        
        'created_at': convert_datetime_to_isoformat(auction_dict.get('created_at')),
        'updated_at': convert_datetime_to_isoformat(auction_dict.get('updated_at')),
        'deleted_at': convert_datetime_to_isoformat(auction_dict.get('deleted_at')),
    }

def get_auction(request, auction_id=None):
    filter_dict = {
        'status__in': [AuctionBase.PRE, AuctionBase.OPEN],
        'deleted_at': None
    }

    if auction_id:
        filter_dict['id'] = auction_id

    order_by = '-created_at'
    if request.GET.get('order_by'):
        order_by = request.GET.get('order_by')

    if request.GET.get('status'):
        filter_dict['status'] = request.GET.get('status')

    if request.GET.get('fruit_type_id'):
        filter_dict['farm__fruit_breed__fruit_type_id'] = request.GET.get('fruit_type_id')

    if request.GET.get('fruit_breed_id'):
        filter_dict['farm__fruit_breed__id'] = request.GET.get('fruit_breed_id')

    if request.GET.get('farm_standard_id'):
        filter_dict['farm__farm_standard__id'] = request.GET.get('farm_standard_id')

    if request.GET.get('province_id'):
        filter_dict['farm__province_id'] = request.GET.get('province_id')

    if request.GET.get('district_id'):
        filter_dict['farm__district_id'] = request.GET.get('district_id')

    if request.GET.get('sub_district_id'):
        filter_dict['farm__sub_district_id'] = request.GET.get('sub_district_id')

    if request.GET.get('postal_code_id'):
        filter_dict['farm__postal_code_id'] = request.GET.get('postal_code_id')

    if request.GET.get('num_area'):
        filter_dict['farm__num_area'] = request.GET.get('num_area')

    if request.GET.get('num_tree'):
        filter_dict['farm__num_tree'] = request.GET.get('num_tree')

    auction_values = AuctionBase.objects.filter(**filter_dict).order_by(order_by).values(
        'id', 'code', 'is_collect_service',
        'expected_harvest_output',
        'expected_harvest_date_time_start', 'expected_harvest_date_time_end',
        'date_time_start', 'date_time_end',
        'price_start', 'price_current',
        'guarantee_used', 'guarantee_price', 'extra_condition',
        'status',
        'created_at', 'updated_at', 'deleted_at',

        'farm__address', 'farm__country', 'farm__district_id', 'farm__farm_standard__name',
        'farm__fruit_breed__name', 'farm__fruit_breed__fruit_type__name', 'farm__id', 'farm__name',
        'farm__num_area', 'farm__num_tree', 'farm__phone_number',
        'farm__postal_code_id', 'farm__province_id', 'farm__road', 'farm__sub_district_id',
        'farm__created_at', 'farm__updated_at', 'farm__deleted_at'
    )
    
    response = None

    auction_list = []
    for auction_dict in auction_values:
        auction_data = get_auction_response(auction_dict)
        auction_data['medias'] = MediaAuctionBase.get_media_auction_base_response_by_auction_id(auction_dict.get('id'))
        auction_list.append(auction_data)
        
    if auction_id:
        response = {}
        if len(auction_list) > 0:
            response = auction_list[0]
    else:
        response = auction_list
        
    return response

class AuctionListView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, auction_id=None):
        auction_list = get_auction(request, auction_id)
        
        if auction_id:
            return Response(auction_list)
        
        auction_list, page = pagination_util.get_pagination_list(self.request, auction_list)

        print(auction_list)
        response = {
            'data': auction_list,
            'page': page
        }
        return Response(response)
