from datetime import datetime
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db import IntegrityError

from auction.models import AuctionBase
from auction.models_media_helper import MediaAuctionBase
from utils.util_time import convert_datetime_to_isoformat


class UserAuctionMediaAPIView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    
    def post(self, request, auction_id=None):
        filter_dict = {
            'draft_farm__user': request.user,
            'deleted_at': None,
            'id': auction_id,
        }
        if AuctionBase.objects.filter(**filter_dict).first() is None:
            return Response('auction not found', status=404)
        
        try:
            data_dict = {
                'auction_base_id': auction_id,
                # 'path': request.data.get('image'),
                'draft_path': request.data.get('image'),
            }
            MediaAuctionBase(**data_dict).save()
            
            return Response('success', status=201)
        
        except:
            pass
        
        return Response('something went wrong', status=400)
    
    def delete(self, request, auction_id=None, media_id=None):
        try:
            filter_dict = {
                'auction_base__draft_farm__user': request.user,
                'auction_base_id': auction_id,
                'id': media_id,
                'deleted_at': None,
            }
            media_auction_base = MediaAuctionBase.objects.filter(**filter_dict).first()
            media_auction_base.deleted_at = datetime.now()
            media_auction_base.save()
            
            return Response('success', status=200)
        
        except:
            pass
        
        return Response('item not found', status=404)