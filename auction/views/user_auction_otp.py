import hashlib
from datetime import datetime, timedelta
from django.utils import timezone
import pytz
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db import IntegrityError
from django.db.models import F
from auction.models import AuctionBase, OTP
from auction.models_media_helper import MediaAuctionBase
from utils.util_time import convert_datetime_to_isoformat, gen_ref
from django.conf import settings


utc = pytz.UTC
EXPRIRED_TIME = settings.EXPRIRED_TIME


def disable_all_related_otp(user, auction_base):
    # disable all related otp data
    return OTP.objects.filter(user=user) \
        .filter(transaction_model=OTP.AUCTION_BASE) \
        .filter(auction_base=auction_base) \
        .update(status=OTP.STATUS_INACTIVE)


def active_auction_data(auction_base):
    if auction_base.status in [AuctionBase.DRAFT]:
        auction_base.status = AuctionBase.PRE
    
    auction_base.farm = auction_base.draft_farm
    
    auction_base.is_collect_service = auction_base.draft_is_collect_service
    auction_base.expected_harvest_output = auction_base.draft_expected_harvest_output
    auction_base.expected_harvest_date_time_start = auction_base.draft_expected_harvest_date_time_start
    auction_base.expected_harvest_date_time_end = auction_base.draft_expected_harvest_date_time_end
    
    auction_base.date_time_start = auction_base.draft_date_time_start
    auction_base.date_time_end = auction_base.draft_date_time_end
    
    auction_base.price_start = auction_base.draft_price_start
    
    auction_base.guarantee_used = auction_base.draft_guarantee_used
    auction_base.guarantee_price = auction_base.draft_guarantee_price
    auction_base.extra_condition = auction_base.draft_extra_condition
    
    auction_base.save()

    # active media data
    MediaAuctionBase.objects.filter(auction_base=auction_base).update(path=F('draft_path'))
    
    return auction_base


class UserAuctionOTPAPIView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    
    def post(self, request, auction_id=None):
        user = request.user
        filter_dict = {
            'draft_farm__user': user,
            'deleted_at': None,
            'id': auction_id,
        }
        auction_base = AuctionBase.objects.filter(**filter_dict).first()
        if auction_base is None:
            return Response('auction not found', status=404)

        disable_all_related_otp(user, auction_base)
        
        ref = gen_ref()
        otp = ref
        expired_at = datetime.now() + EXPRIRED_TIME
        transaction_type = OTP.CREATE if auction_base.status in [AuctionBase.DRAFT] else OTP.UPDATE
        
        otp_data = {
            'ref': ref,
            'otp': otp,
            'transaction_type': transaction_type,
            'transaction_model': OTP.AUCTION_BASE,
            'status': OTP.STATUS_ACTIVE,
            'user': user,
            'auction_base': auction_base,
            'expired_at': expired_at
        }

        try:
            otp_queryset = OTP(**otp_data)
            otp_queryset.save()
            
            otp_data = {
                'id': otp_queryset.id,
                'ref': otp_queryset.ref,
                'auction_id': otp_queryset.auction_base.id,
                'expired_at': convert_datetime_to_isoformat(expired_at)
            }
            
            return Response({'data': otp_data}, status=201)
        
        except:
            pass

        return Response('something went wrong', status=400)
    
    def put(self, request, auction_id=None):
        user = request.user
        filter_dict = {
            'draft_farm__user': user,
            'deleted_at': None,
            'id': auction_id,
        }
        auction_base = AuctionBase.objects.filter(**filter_dict).first()
        if auction_base is None:
            return Response('auction not found', status=404)
        
        params = request.data
        ref = params.get('ref')
        otp = params.get('otp')
        
        if ref is None or otp is None:
            return Response('invalid otp', status=404)
        
        otp_queryset = OTP.objects.filter(user=user) \
            .filter(transaction_model=OTP.AUCTION_BASE) \
            .filter(auction_base=auction_base) \
            .filter(status=OTP.STATUS_ACTIVE) \
            .filter(ref=ref) \
            .filter(otp=otp) \
            .order_by('created_at') \
            .last()
        
        if otp_queryset is None:
            return Response('invalid otp', status=404)
        
        if otp_queryset.expired_at < timezone.now():
            return Response('otp expired', status=404)

        try:
            # mark as verify
            disable_all_related_otp(user, auction_base)
            otp_queryset.status = OTP.STATUS_VERIFY
            otp_queryset.save()
            
            # active auction base and media data
            active_auction_data(auction_base)
            
            return Response('success', status=200)
            
        except:
            pass
            
        return Response('something went wrong', status=400)
