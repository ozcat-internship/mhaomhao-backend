import hashlib
from datetime import datetime, timedelta
import pytz
from django.utils import timezone
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.db.models import Max
from django.db import IntegrityError
from django.db.models import F
from auction.models import AuctionBase, OTP, AuctionTransaction
from auction.models_media_helper import MediaAuctionBase
from utils.util_time import convert_datetime_to_isoformat, gen_ref
from django.conf import settings


utc = pytz.UTC
EXPRIRED_TIME = settings.EXPRIRED_TIME


def disable_all_related_otp(user, auction_transaction):
    # disable all related otp data
    return OTP.objects.filter(user=user) \
        .filter(transaction_model=OTP.AUCTION_TRANSACTION) \
        .filter(auction_transaction=auction_transaction) \
        .update(status=OTP.STATUS_INACTIVE)


class UserBidOTPAPIView(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication, TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, auction_transaction_id=None):
        user = request.user
        filter_dict = {
            'bid_user': user,
            'deleted_at': None,
            'id': auction_transaction_id,
        }
        auction_transaction = AuctionTransaction.objects.filter(**filter_dict).first()
        if auction_transaction is None:
            return Response('auction transaction not found', status=404)

        disable_all_related_otp(user, auction_transaction)

        ref = gen_ref()
        otp = ref
        expired_at = datetime.now() + EXPRIRED_TIME
        transaction_type = OTP.CREATE

        otp_data = {
            'ref': ref,
            'otp': otp,
            'transaction_type': transaction_type,
            'transaction_model': OTP.AUCTION_TRANSACTION,
            'status': OTP.STATUS_ACTIVE,
            'user': user,
            'auction_transaction': auction_transaction,
            'expired_at': expired_at
        }

        try:
            otp_queryset = OTP(**otp_data)
            otp_queryset.save()

            otp_data = {
                'id': otp_queryset.id,
                'ref': otp_queryset.ref,
                'auction_transaction_id': otp_queryset.auction_transaction.id,
                'expired_at': convert_datetime_to_isoformat(expired_at)
            }

            return Response({'data': otp_data}, status=201)

        except:
            pass

        return Response('something went wrong', status=400)

    def put(self, request, auction_transaction_id=None):
        user = request.user
        filter_dict = {
            'bid_user': user,
            'deleted_at': None,
            'id': auction_transaction_id,
        }
        auction_transaction = AuctionTransaction.objects.filter(**filter_dict).first()
        if auction_transaction is None:
            return Response('auction transaction not found', status=404)

        params = request.data
        ref = params.get('ref')
        otp = params.get('otp')

        if ref is None or otp is None:
            return Response('invalid otp', status=404)

        otp_queryset = OTP.objects.filter(user=user) \
            .filter(transaction_model=OTP.AUCTION_TRANSACTION) \
            .filter(auction_transaction=auction_transaction) \
            .filter(status=OTP.STATUS_ACTIVE) \
            .filter(ref=ref) \
            .filter(otp=otp) \
            .order_by('created_at') \
            .last()

        if otp_queryset is None:
            return Response('invalid otp', status=404)

        if otp_queryset.expired_at < timezone.now():
            return Response('otp expired', status=404)

        # try:
        if True:
            # mark as verify
            disable_all_related_otp(user, auction_transaction)
            otp_queryset.status = OTP.STATUS_VERIFY
            otp_queryset.save()

            # active auction transaction

            # get current bid price
            auction_base = auction_transaction.auction_base
            bid_price = auction_transaction.bid_price

            filter_dict = {
                'auction_base': auction_base,
                'transaction_status__in': [AuctionTransaction.BUY],
                'deleted_at': None,
            }
            price = AuctionTransaction.objects.filter(**filter_dict).aggregate(bid_price=Max('bid_price'))
            current_transaction_price = price.get('bid_price')

            # validate current transaction price
            if current_transaction_price is not None:
                if bid_price <= current_transaction_price:
                    return Response('bid price should more than current price', status=404)
            # validate minimum price
            if bid_price < auction_base.price_start:
                return Response('bid price should more than starting price', status=404)

            # update price
            auction_base.current_price = bid_price

            auction_transaction.transaction_status = AuctionTransaction.BUY
            auction_transaction.bid_date_time = datetime.now()
            auction_transaction.save()

            return Response('success', status=200)

        # except:
        #     pass

        return Response('something went wrong', status=400)

