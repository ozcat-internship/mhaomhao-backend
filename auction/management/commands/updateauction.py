from django.core.management.base import BaseCommand, CommandError
from dateutil import tz
from datetime import datetime, timedelta
from auction.models import AuctionBase

est = tz.gettz('Asia/Bangkok')

class Command(BaseCommand):
    help = 'Update(open-close) auctions by start-end datetime'

    def handle(self, *args, **options):
        tobeOpenAuctions = query_tobe_open_auction()
        tobeOpenAuctions.update(status=AuctionBase.OPEN)
        self.stdout.write(self.style.SUCCESS('Successfully open scheduled auctions'))
        tobeCloseAuctions = query_tobe_close_auction()
        tobeCloseAuctions.update(status=AuctionBase.CLOSE)
        self.stdout.write(self.style.SUCCESS('Successfully close expired auctions'))

def query_tobe_open_auction():
    today = datetime.utcnow().date()
    start = datetime(today.year, today.month, today.day, tzinfo=tz.tzutc()).astimezone(est)
    end = start + timedelta(1)

    filter_dict = {
        'date_time_start__lte': start,
        'date_time_end__gte': end,
        'status': AuctionBase.PRE,
        'deleted_at': None,
    }
    return AuctionBase.objects.filter(**filter_dict)

def query_tobe_close_auction():
    today = datetime.utcnow().date()
    start = datetime(today.year, today.month, today.day, tzinfo=tz.tzutc()).astimezone(est)
    end = start + timedelta(1)

    filter_dict = {
        'date_time_start__lte': start,
        'date_time_end__gte': end,
        'status': AuctionBase.OPEN,
        'deleted_at': None,
    }
    return AuctionBase.objects.filter(**filter_dict)