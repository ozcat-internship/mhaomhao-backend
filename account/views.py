from django.contrib.auth import get_user_model
from rest_framework import generics, permissions, status, views
from account import serializers
from rest_framework.response import Response
from service.sendgrid_service import send_email
from utils import base36
from utils.permissions import IsEmailVerify
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope

User = get_user_model()


class UserRegistrationAPIView(generics.CreateAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.RegisterSerializer
    queryset = User.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save()
            send_email(user.email, 'ctrlup verify', 'http://localhost:8000/' + user.token_email_verify)
            return Response({'success': True}, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserEmailVerificationAPIView(views.APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, verification_key):
        activated_user = self.activate(verification_key)
        if activated_user:
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def activate(self, verification_key):
        return User.objects.activate_user(verification_key)


class PasswordResetAPIView(views.APIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.PasswordResetSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.create_reset_password_token()
            uid = base36.base36encode(user.pk)
            send_email(user.email,
                       'ctrlup verify',
                       'http://localhost:8000/account/reset/?' + 'id='
                       + uid + "&token=" + user.token_reset_password)
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_200_OK)


class PasswordResetConfirmView(views.APIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = serializers.PasswordResetConfirmSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data,
            context={
                'uidb64': request.query_params.get('id'),
                'token': request.query_params.get('token')
            })

        if serializer.is_valid(raise_exception=True):
            new_password = serializer.validated_data.get('new_password')
            user = serializer.user
            user.set_password(new_password)
            user.token_reset_password = None
            user.save()
            return Response(status=status.HTTP_200_OK)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def response_error_handler(request, exception=None):
    return Response('Error handler content', status=403)


class UserProfileAPIView(generics.RetrieveAPIView, generics.UpdateAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = serializers.UserSerializer

    def get_object(self):
        return self.request.user

    def put(self, request, *args, **kwargs):
        context = {
            'user': request.user,
            'request': request
        }
        serializer = self.serializer_class(
            request.user, data=request.data, context=context
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AddressAPIView(generics.CreateAPIView, generics.UpdateAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = serializers.MutateAddressSerializer

    def get_object(self, user):
        try:
            return user.address
        except User.DoesNotExist:
            raise

    def post(self, request, *args, **kwargs):
        context = {
            'user': request.user,
            'request': request
        }
        serializer = self.serializer_class(
            data=request.data, context=context
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        context = {
            'user': request.user,
            'request': request
        }
        address = self.get_object(request.user)
        print(address)
        serializer = self.serializer_class(
            request.user.address, data=request.data, context=context
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ShippingAddressAPIView(generics.CreateAPIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = serializers.MutateShippingAddressSerializer

    def get_object(self, user):
        try:
            return user.address
        except User.DoesNotExist:
            raise

    def post(self, request, *args, **kwargs):
        context = {
            'user': request.user,
            'request': request
        }
        serializer = self.serializer_class(
            data=request.data, context=context
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        context = {
            'user': request.user,
            'request': request
        }
        serializer = self.serializer_class(
            request.user.shipping_address, data=request.data, context=context
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CreditCardAPIView(generics.CreateAPIView, generics.DestroyAPIView, generics.UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.CreditCardSerializer

    def post(self, request, *args, **kwargs):
        context = {
            'user': request.user,
            'request': request
        }
        serializer = self.serializer_class(
            data=request.data, context=context
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, *args, **kwargs):
        context = {
            'user': request.user,
            'request': request
        }
        serializer = serializers.UpdateCreditCardSerializer(
            request.user, data=request.data, context=context
        )
        # serializer = self.serializer_class(
        #     request.user, data=request.data, context=context
        # )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        context = {
            'user': request.user,
            'request': request
        }
        serializer = self.serializer_class(
            data=request.data, context=context
        )
        if serializer.is_valid(raise_exception=True):
            data = serializer.delete(serializer.validated_data)
            return Response(data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CreditCardDefaultAPIView(generics.UpdateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = serializers.CreditCardDefaultSerializer

    def put(self, request, *args, **kwargs):
        context = {
            'user': request.user,
            'request': request
        }
        serializer = self.serializer_class(
            request.user, data=request.data, context=context
        )
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
