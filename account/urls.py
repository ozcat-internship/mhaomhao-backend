from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^register/$', views.UserRegistrationAPIView.as_view(), name='register'),
    url(r'^verify/email/(?P<verification_key>.+)/$',views.UserEmailVerificationAPIView.as_view(), name='email_verify'),
    url(r'^password_reset/$',
        views.PasswordResetAPIView.as_view(),
        name='password_change'),
    url(r'^reset/$',
        views.PasswordResetConfirmView.as_view(),
        name='password_change'),
    url(r'^me/$',
        views.UserProfileAPIView.as_view(),
        name='user_profile'),

    url(r'^address/$',
        views.AddressAPIView.as_view(),
        name='address'),

    url(r'^shipping-address/$',
        views.ShippingAddressAPIView.as_view(),
        name='shipping_address'),

    url(r'^credit-card/$',
        views.CreditCardAPIView.as_view(),
        name='credit_card'),

    url(r'^credit-card/active/$',
        views.CreditCardDefaultAPIView.as_view(),
        name='credit_card_default'),

]
