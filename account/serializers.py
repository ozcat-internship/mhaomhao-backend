import hashlib

from rest_framework import serializers
from django.contrib.auth import get_user_model
from django.utils.crypto import get_random_string
from utils import base36, address as address_util
from .models import Address
from pprint import pprint
from service.omise_service import create_customer, \
    add_card_to_customer, \
    retrieve_card_list, \
    remove_card_to_customer, \
    edit_card
import json

User = get_user_model()


class RegisterSerializer(serializers.ModelSerializer):
    username = serializers.EmailField(
        required=True,
    )
    first_name = serializers.CharField(
        required=True
    )

    last_name = serializers.CharField(
        required=True
    )
    password = serializers.CharField(
        style={'input_type': 'password'},
        write_only=True
    )

    class Meta:
        model = User
        fields = ('username', 'password', 'first_name', 'last_name')

    def validate_email(self, value):
        if User.objects.filter(email=value).exists():
            raise serializers.ValidationError("Email already exists.")
        return value

    def validate_username(self, value):
        if User.objects.filter(username=value).exists():
            raise serializers.ValidationError("Email already exists.")
        return value

    def create(self, validated_data):
        user_data = {
            'username': validated_data.get('username'),
            'email': validated_data.get('username'),
            'password': validated_data.get('password'),
            'first_name': validated_data.get('first_name'),
            'last_name': validated_data.get('last_name')
        }
        user = User.objects.create_user(**user_data)
        hash_input = (get_random_string(5) + user.username).encode('utf-8')
        user.token_email_verify = hashlib.sha1(hash_input).hexdigest()
        user.save()
        return user


class PasswordResetSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required=True
    )

    def create_reset_password_token(self):
        user = self.get_user(self['email'].value)
        if user:
            hash_input = (get_random_string(5) + user.username).encode('utf-8')
            user.token_reset_password = hashlib.sha1(hash_input).hexdigest()
            user.save()
            return user

    def validate_email(self, value):
        # if not User.objects.filter(email=value).exists():
        #     raise serializers.ValidationError("Email not already exists.")
        return value

    def get_user(self, email):
        try:
            user = User.objects.get(email=email)
        except:
            return None
        return user


class PasswordResetConfirmSerializer(serializers.Serializer):

    def __init__(self, *args, **kwargs):
        context = kwargs['context']
        uidb64, token = context['uidb64'], context.get('token')
        if uidb64 and token:
            uid = base36.base36decode(uidb64)
            self.user = self.get_user(uid)
            self.valid_attempt = self.check_token(self.user.token_reset_password, token)
        else:
            self.valid_attempt = False
        super(PasswordResetConfirmSerializer, self).__init__(*args, **kwargs)

    def get_user(self, uid):
        try:
            user = User._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, User.DoesNotExist):
            user = None
        return user

    def check_token(self, user_token, token):
        if user_token == token:
            return True
        return False

    new_password = serializers.CharField(
        style={'input_type': 'password'},
        label="New Password",
        write_only=True
    )

    new_password_2 = serializers.CharField(
        style={'input_type': 'password'},
        label="Confirm New Password",
        write_only=True
    )

    def validate_new_password_2(self, value):
        data = self.get_initial()
        new_password = data.get('new_password')
        if new_password != value:
            raise serializers.ValidationError("Passwords doesn't match.")
        return value

    def validate(self, data):
        if not self.valid_attempt:
            raise serializers.ValidationError("Operation not allowed.")
        return data


class AddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = Address
        fields = ['id',
                  'first_name',
                  'last_name',
                  'address_detail',
                  'road',
                  'country',
                  'province_id',
                  'district_id',
                  'sub_district_id',
                  'postal_code_id',
                  'phone_number']

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        provinces = dict(enumerate(address_util.find_province(instance.province_id)))
        districts = dict(enumerate(address_util.find_district(instance.district_id)))
        sub_districts = dict(enumerate(address_util.find_sub_district(instance.sub_district_id)))
        postal_codes = dict(enumerate(address_util.find_postal_code(instance.postal_code_id)))
        print("----address to_representration-----")
        ret['province'] = provinces.get(0, {}).get('name', {}) or None
        ret['districts'] = districts.get(0, {}).get('name', {}) or None
        ret['sub_districts'] = sub_districts.get(0, {}).get('name', {}) or None
        ret['postal_code'] = postal_codes.get(0, {}).get('code') or None
        return ret

    def validate_address(self, province_id, district_id, sub_district_id, postal_code_id):
        if not address_util.is_find_province(province_id):
            raise serializers.ValidationError({
                "detail": "Not Found Province Id"
            })
        if not address_util.is_find_district(district_id):
            raise serializers.ValidationError({
                "detail": "Not Found District Id"
            })
        if not address_util.is_find_sub_district(sub_district_id):
            raise serializers.ValidationError({
                "detail": "Not Found Sub District Id"
            })
        if not address_util.is_find_postal_code(postal_code_id):
            raise serializers.ValidationError({
                "detail": "Not Found Postal Code Id"
            })


class MutateAddressSerializer(AddressSerializer):
    class Meta:
        model = Address
        fields = ['id',
                  'first_name',
                  'last_name',
                  'address_detail',
                  'road',
                  'country',
                  'province_id',
                  'district_id',
                  'sub_district_id',
                  'postal_code_id',
                  'phone_number']

    def create(self, validated_data):
        user = self.context.get('user', None)
        if not user.address:
            # address_data = validated_data.pop('address')
            address = Address.objects.create(**validated_data)
            print("----address----")
            print(address)
            user.address = address
            user.save()
        return user

    def update(self, instance, validated_data):
        user = self.context.get('user', None)
        address, created = Address.objects.update_or_create(
            id=instance.id, defaults=validated_data
        )
        if not created:
            user.address = address
            user.save()
        # if not user.address:
        #     address = Address.objects.create(**validated_data)
        #     user.address = address
        #     user.save()
        # else:
        #     print(validated_data)
        #     print(instance)
        #     instance.first_name = validated_data.get("first_name")
        #     instance.save()
        return user

    def validate(self, attrs):
        province_id = attrs.get('province_id')
        district_id = attrs.get('district_id')
        sub_district_id = attrs.get('sub_district_id')
        postal_code_id = attrs.get('postal_code_id')
        self.validate_address(province_id, district_id, sub_district_id, postal_code_id)
        return attrs

    def to_representation(self, instance):
        serializer = UserSerializer(instance)
        return serializer.data


class MutateShippingAddressSerializer(AddressSerializer):
    def create(self, validated_data):
        user = self.context.get('user', None)
        if not user.shipping_address:
            shipping_address = Address.objects.create(**validated_data)
            user.shipping_address = shipping_address
            user.save()
        return user

    def update(self, instance, validated_data):
        user = self.context.get('user', None)
        address, created = Address.objects.update_or_create(
            id=instance.id, defaults=validated_data
        )
        if not created:
            user.shipping_address = address
            user.save()
        # instance.__dict__.update(validated_data)
        # instance.save()
        return user

    def validate(self, attrs):
        province_id = attrs.get('province_id')
        district_id = attrs.get('district_id')
        sub_district_id = attrs.get('sub_district_id')
        postal_code_id = attrs.get('postal_code_id')
        self.validate_address(province_id, district_id, sub_district_id, postal_code_id)
        return attrs

    def to_representation(self, instance):
        serializer = UserSerializer(instance)
        return serializer.data


# class CreateAddressSerializer(serializers.Serializer):
#     address = AddressSerializer()
#     shipping_address = AddressSerializer()
#
#     def create(self, validated_data):
#         user = self.context.get('user', None)
#         if not user.address:
#             address_data = validated_data.pop('address')
#             address = Address.objects.create(**address_data)
#             user.address = address
#         if not user.shipping_address:
#             shipping_address_data = validated_data.pop('shipping_address')
#             shipping_address = Address.objects.create(**shipping_address_data)
#             user.shipping_address = shipping_address
#         user.save()
#         return user
#
#     def to_representation(self, instance):
#         pprint(vars(instance))
#         serializer = UserSerializer(instance)
#         return serializer.data
#
#     class Meta:
#         fields = ['address', 'shipping_address']


class UserSerializer(serializers.ModelSerializer):
    address = AddressSerializer(required=False, read_only=True)
    shipping_address = AddressSerializer(required=False, read_only=True)

    class Meta:
        model = User
        fields = ['id',
                  'email',
                  'first_name',
                  'last_name',
                  'is_email_verify',
                  'is_bank_verify',
                  'is_identity_id_verify',
                  'phone_number',
                  'bank_name',
                  'bank_account',
                  'bank_account_url',
                  'identity_id',
                  'identity_id_url',
                  'birth_date',
                  'address',
                  'shipping_address', ]
        # extra_kwargs = {
        #     'id': {'read_only': True},
        #     'is_email_verify': {'read_only': True},
        #     'email': {'read_only': True},
        #     'is_bank_verify': {'read_only': True},
        # }
        read_only_fields = ('is_email_verify', 'is_bank_verify', 'email', 'id',)

    def update(self, instance, validated_data):
        print("update")
        user = self.context.get('user', None)
        user.__dict__.update(validated_data)
        user.save()
        return user

    # def get_fields(self, *args, **kwargs):
    #     print("get fields")
    #     user = self.context.get('user', None)
    #     print(user)
    #     return super(UserSerializer, self).get_fields(*args, **kwargs)

    def to_representation(self, instance):
        def add_default_index(data):
            idx, value = data
            if instance.card_default_index == (idx + 1):
                return dict(value, is_default=True)
            return dict(value, is_default=False)

        if instance.omise_customer_id:
            cards = retrieve_card_list(instance.omise_customer_id)
        ret = super().to_representation(instance)
        try:
            r_cards = list(map(add_default_index, enumerate(cards)))
            print(enumerate(cards))
            ret['cards'] = r_cards
        except:
            ret['cards'] = None
        return ret


class CreditCardSerializer(serializers.Serializer):
    card_id = serializers.CharField(
        required=True,
        write_only=True
    )

    def create(self, validated_data):
        user = self.context.get('user', None)
        card_id = validated_data.get("card_id")
        try:
            if not user.omise_customer_id:
                customer = create_customer(user.email, card_id)
                pprint(vars(customer))
                user.omise_customer_id = customer.id
                user.card_default_index = 1
                user.save()
            else:
                print(user.omise_customer_id)
                customer = add_card_to_customer(user.omise_customer_id, card_id)
                if len(customer.cards) == 1:
                    user.card_default_index = 1
                    user.save()

            return user
        except:
            raise serializers.ValidationError({
                "detail": "Can not add Card."
            })

    def delete(self, validated_data):
        user = self.context.get('user', None)
        card_id = validated_data.get("card_id")
        try:
            if not user.omise_customer_id:
                raise serializers.ValidationError({
                    "detail": "Not Found Card."
                })
            else:
                cards = retrieve_card_list(user.omise_customer_id)
                card_default_index = next((index for (index, d) in enumerate(cards) if d["id"] == card_id), None)
                if (card_default_index + 1) == user.card_default_index:
                    user.card_default_index = 1
                    user.save()
                remove_card_to_customer(user.omise_customer_id, card_id)
                return self.to_representation(user)
        except:
            raise serializers.ValidationError({
                "detail": "Not Found Card."
            })
        return self.to_representation(user)

    def to_representation(self, instance):
        serializer = UserSerializer(instance)
        return serializer.data

class UpdateCreditCardSerializer(serializers.Serializer):
    card_id = serializers.CharField(
        required=True,
        write_only=True
    )
    name = serializers.CharField(
        write_only=True,
        required=True
    )
    exp_month = serializers.IntegerField(
        write_only=True,
        required=True
    )
    exp_year = serializers.IntegerField(
        write_only=True,
        required=True
    )

    def update(self, instance, validated_data):
        user = self.context.get('user', None)
        card_id = validated_data.get("card_id")
        name = validated_data.get("name")
        exp_month = validated_data.get("exp_month")
        exp_year = validated_data.get("exp_year")
        if user.omise_customer_id:
            edit_card(user.omise_customer_id, card_id, exp_month, exp_year, name)
        return user

    def to_representation(self, instance):
        serializer = UserSerializer(instance)
        return serializer.data


class CreditCardDefaultSerializer(serializers.Serializer):
    card_id = serializers.CharField(
        required=True,
        write_only=True
    )

    def update(self, instance, validated_data):
        user = self.context.get('user', None)
        card_id = validated_data.get("card_id")
        if user.omise_customer_id:
            cards = retrieve_card_list(user.omise_customer_id)
            card_default_index = next((index for (index, d) in enumerate(cards) if d["id"] == card_id), None)
            if card_default_index != None:
                user.card_default_index = card_default_index + 1
                user.save()
        return user

    def to_representation(self, instance):
        serializer = UserSerializer(instance)
        return serializer.data
