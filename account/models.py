import os
import re
import hashlib
from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import ObjectDoesNotExist

token_generator = default_token_generator

SHA1_RE = re.compile('^[a-f0-9]{40}$')


# Create your models here.

class UserProfileRegistrationManager(UserManager):
    def activate_user(self, verification_key):
        if SHA1_RE.search(verification_key.lower()):
            try:
                user = self.get(token_email_verify=verification_key)
            except ObjectDoesNotExist:
                return None
            user.token_email_verify = None
            user.is_email_verify = True
            user.save()
            return user
        return None


class Address(models.Model):
    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=150, blank=False)
    address_detail = models.TextField(blank=True)
    road = models.CharField(max_length=150, blank=True)
    country = models.CharField(max_length=150, blank=False)
    province_id = models.PositiveIntegerField(null=True, blank=True)
    district_id = models.PositiveIntegerField(null=True, blank=True)
    sub_district_id = models.PositiveIntegerField(null=True, blank=True)
    postal_code_id = models.PositiveIntegerField(null=True, blank=True)
    phone_number = models.CharField(max_length=30, blank=False)


def get_id_card_image_path(instance, filename):
    return os.path.join('customer_credential', str(instance.id), 'id_card', filename)


def get_bank_account_image_path(instance, filename):
    return os.path.join('customer_credential', str(instance.id), 'bank_account', filename)


class User(AbstractUser):
    phone_number = models.CharField(max_length=30, blank=True)
    bank_name = models.CharField(max_length=30, blank=True)
    bank_account = models.CharField(max_length=30, blank=True)
    bank_account_url = models.ImageField(upload_to=get_bank_account_image_path, blank=True)

    identity_id = models.CharField(max_length=30, blank=True)
    identity_id_url = models.ImageField(upload_to=get_id_card_image_path, blank=True)

    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    is_email_verify = models.BooleanField(null=False, blank=False, default=False)
    is_bank_verify = models.BooleanField(null=False, blank=False, default=False)
    is_identity_id_verify = models.BooleanField(null=False, blank=False, default=False)
    token_email_verify = models.CharField(null=True, max_length=500, blank=True)
    token_reset_password = models.CharField(null=True, max_length=500, blank=True)
    address = models.ForeignKey(Address, null=True, on_delete=models.SET_NULL, related_name="address", blank=True)
    shipping_address = models.ForeignKey(Address, null=True, on_delete=models.SET_NULL, related_name="shipping_address", blank=True)
    omise_customer_id = models.CharField(max_length=30, blank=True)
    card_default_index = models.IntegerField(default=0)

    objects = UserProfileRegistrationManager()
