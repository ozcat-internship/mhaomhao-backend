#FROM python:3.6
#
#WORKDIR /app
#
## By copying over requirements first, we make sure that Docker will cache
## our installed requirements rather than reinstall them on every build
#COPY requirements.txt /app/requirements.txt
#RUN pip install -r requirements.txt
#
## Now copy in our code, and run it
#COPY . /app
#EXPOSE 8000
#CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]

# Pull base image
FROM python:3.7-slim

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN apt-get update && apt-get install -y gcc unixodbc-dev
# Set work directory
WORKDIR /code

# Install dependencies
#RUN pip install pipenv
#COPY ./Pipfile /code/Pipfile
#RUN pipenv install --system --skip-lock
RUN pip install --upgrade pip
COPY requirements.txt /code/requirements.txt
RUN pip install -r requirements.txt
# Copy project
COPY . /code/